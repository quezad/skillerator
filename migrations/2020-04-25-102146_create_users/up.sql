-- Your SQL goes here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user" (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    email TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL DEFAULT '123',
    is_applicant BOOLEAN NOT NULL  DEFAULT TRUE,
    session_token TEXT,
    created_at TIMESTAMP   NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP
);

INSERT INTO "user" (
    email,
    password,
    is_applicant
)
VALUES
    (
        'paul@asap.de',
        '123',
        false
    ),
    (
        'kostova@outlook.com',
        '123',
        true
    ),
    (
        'quezad@geeklord.ninja',
        '123',
        true
    );
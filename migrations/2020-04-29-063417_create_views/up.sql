-- Your SQL goes here
CREATE VIEW detailed_profiles AS
    SELECT profiles.id AS detail_id,
           applicant_id AS detail_applicant_id,
           recruiter_id AS detail_recruiter_id,
           first_name AS detail_first_name,
           last_name AS detail_last_name,
           date_of_birth AS detail_date_of_birth,
           years_experience AS detail_years_experience,
           prof.description AS detail_work_field,
           salary AS detail_salary,
           visa.description AS detail_visa_type,
           loc.description AS detail_work_location,
           countries.icon AS detail_country_icon,
           countries.description AS detail_country_name,
           steps.description AS detail_current_step,
           steps.for_applicant AS detail_for_applicant
    FROM profiles
         INNER JOIN work_locations AS loc
                    ON profiles.work_location_id = loc.id
         INNER JOIN visa_types AS visa
                    ON profiles.visa_type_id = visa.id
         INNER JOIN work_fields AS prof
                    ON profiles.profession_id = prof.id
         INNER JOIN countries
                    ON profiles.country = countries.id
         INNER JOIN  process_steps AS steps
                    ON profiles.process_step_id = steps.id
;
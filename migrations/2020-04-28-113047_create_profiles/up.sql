-- Your SQL goes here
CREATE TABLE work_fields(
    id INTEGER PRIMARY KEY,
    description TEXT NOT NULL
);

CREATE TABLE visa_types(
    id SERIAL PRIMARY KEY,
    description VARCHAR NOT NULL
);

CREATE TABLE work_locations(
   id SERIAL PRIMARY KEY,
   description VARCHAR NOT NULL
);

CREATE TABLE process_steps (
    id INTEGER PRIMARY KEY,
    for_applicant BOOLEAN NOT NULL DEFAULT TRUE,
    description VARCHAR NOT NULL
);

CREATE TABLE profiles(
    applicant_id UUID NOT NULL REFERENCES "user"(id),
    recruiter_id UUID NOT NULL REFERENCES "user"(id),
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    first_name VARCHAR,
    last_name VARCHAR,
    date_of_birth DATE,
    years_experience NUMERIC(4),
    profession_id INTEGER REFERENCES work_fields(id),
    salary NUMERIC(8,2),
    visa_type_id INTEGER REFERENCES visa_types(id),
    work_location_id INTEGER REFERENCES work_locations(id),
    country VARCHAR(64) REFERENCES countries(id),
    process_step_id INTEGER REFERENCES process_steps(id),
    PRIMARY KEY (applicant_id, recruiter_id, id)
);

INSERT INTO process_steps( id, for_applicant, description)
VALUES
(1, FALSE, 'Create Profile'),
(2, FALSE, 'Send Link to Applicant'),
(3, TRUE, 'Applicant Signs up'),
(4, TRUE, 'Send Power of Attorney'),
(5, FALSE, 'Schedule Appointment at Embassy'),
(6, TRUE, 'Apply for Visa'),
(7, TRUE, 'Journey to Country'),
(8, TRUE, 'Apply for Work Permit');

INSERT INTO work_fields(
    id, description
)VALUES
(
    0, 'Other'
),
(
    1, 'Managers'
),
(
    112,
    'Managing directors and chief executives'
),
(
    1212,
    'Human resource managers'
),
(
    122,
    'Sales, marketing, PR and development managers'),
(
    19,
    'Other managers'
),
(
    2,
    'Professionals'
),
(
    21,
    'Science and engineering professionals'
),
(
    211,
    'Physical and earth science professionals'
),
(
    2166,
    'Designers'
),
(
    212,
    'Mathematicians, actuaries and statisticians'
),
(
    213,
    'Life science professionals'
),
(
    214,
    'Engineering professionals (excluding electrotechnology)'
),
(
    215,
    'Electrotechnology engineers'
),
(
    216,
    'Architects, planners, surveyors and designers'
),
(
    22,
    'Health professionals'
),
(
    23,
    'Teaching professionals'
),
(
    24,
    'Business and administration professionals'
),
(
    241,
    'Finance professionals'
),
(
    242,
    'Administration professionals'
),
(
    243,
    'Sales, marketing and public relations professionals'
),
(
    25,
    'Software developers and software engineers'
),
(
    26,
    'Legal, social and cultural professionals'
),
(
    263,
    'Social and religious professionals'
),
(
    264,
    'Authors, journalists and linguists'
),
(
    265,
    'Creative and performing artists'
),
(
    3,
    'Technicians and associate professionals'
),
(
    31,
    'Science and engineering associate professionals'
),
(
    311,
    'Physical and engineering science technicians'
),
(
    312,
    'Mining, manufacturing and construction supervisors'
),
(
    313,
    'Process control technicians'
),
(
    314,
    'Life science technicians and related associate professionals'
),
(
    315,
    'Ship and aircraft controllers and technicians'
),
(
    32,
    'Health associate professionals'
),
(
    33,
    'Business and administration associate professionals'
),
(
    34,
    'Legal, social, cultural and related associate professionals'
),
(
    35,
    'Information and communications technicians'
),
(
    4,
    'Clerical support workers'
),
(
    5,
    'Service and sales workers'
),
(
    51,
    'Personal service workers'
),
(
    511,
    'Travel attendants, conductors and guides'
),
(
    512,
    'Cooks'
),
(
    515,
    'Building and housekeeping supervisors'
),
(
    516,
    'Other personal services workers'
),
(
    52,
    'Sales workers'
),
(
    54,
    'Protective services workers'
),
(
    6,
    'Skilled agricultural, forestry and fishery workers'
),
(
    61,
    'Market-oriented skilled agricultural workers'
),
(
    62,
    'Market-oriented skilled forestry, fishery and hunting workers'
),
(
    63,
    'Subsistence farmers, fishers, hunters and gatherers'
),
(
    7,
    'Craft and related trades workers'
),
(
    71,
    'Building and related trades workers, excluding electricians'
),
(
    72,
    'Metal, machinery and related trades workers'
),
(
    73,
    'Handicraft and printing workers'
),
(
    74,
    'Electrical and electronic trades workers'
),
(
    75,
    'Food processing, wood working, garment and other craft and related trades workers'
),
(
    8,
    'Plant and machine operators, and assemblers'
),
(
    81,
    'Stationary plant and machine operators'
),
(
    83,
    'Drivers and mobile plant operators'
),
(
    9,
    'Elementary occupations'
);

INSERT INTO visa_types(
    description
)VALUES(
    'Blue Card'
),
(
    'Work Permit'
),
(
    'Permit for Professional Qualifications'
),
(
    'Permit for Research and Investigation'
),
(
    'Freelancer Permit'
),
(
    'Family Reunion - Spouse / Children'
);

INSERT INTO work_locations(
    description
)
VALUES (
   'Baden-Württemberg'
),
(
   'Bayern'
),
(
   'Berlin'
),
(
   'Brandenburg'
),
(
   'Bremen'
),
(
   'Hamburg'
),
(
   'Hessen'
),
(
   'Niedersachsen'
),
(
   'Mecklenburg-Vorpommern'
),
(
   'Nordrhein-Westfalen'
),
(
   'Rheinland-Pfalz'
),
(
   'Saarland'
),
(
   'Sachsen'
),
(
   'Sachsen-Anhalt'
),
(
   'Schleswig-Holstein'
),
(
   'Thüringen'
);

INSERT INTO profiles(
    applicant_id, recruiter_id, first_name, last_name, date_of_birth, years_experience, profession_id, salary, visa_type_id, work_location_id, country, process_step_id
)
SELECT applicant.id, recruiter.id, 'Ekaterina', 'Kostova', '1987-08-19', '12', 74, 43056.00, 1, 3, 'RU', 5 FROM
    "user" AS applicant,
    "user" as recruiter WHERE
        recruiter.email = 'paul@asap.de' AND
        applicant.email = 'kostova@outlook.com'
;

INSERT INTO profiles(
    applicant_id, recruiter_id, first_name, last_name, date_of_birth, years_experience, profession_id, salary, visa_type_id, work_location_id, country, process_step_id
)
SELECT applicant.id, recruiter.id, 'Marcos', 'Quezada', '1982-07-30', '15', 25, 65000.00, 2, 10, 'MX', 8 FROM
    "user" AS applicant,
    "user" as recruiter WHERE
        recruiter.email = 'paul@asap.de' AND
        applicant.email = 'quezad@geeklord.ninja'
;

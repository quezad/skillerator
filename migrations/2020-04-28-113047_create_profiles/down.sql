-- This file should undo anything in `up.sql`
DROP TABLE profiles;
DROP TABLE work_locations;
DROP TABLE visa_types;
DROP TABLE work_fields;
DROP TABLE process_steps;
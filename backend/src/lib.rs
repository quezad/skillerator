//! The backend library
#![deny(missing_docs)]

mod database;
mod http;
mod server;
mod token;
mod auth;
mod catalogs;
mod user;
mod profile;

pub use crate::server::Server;

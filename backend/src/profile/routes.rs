use actix_web::{error::{Error, ErrorInternalServerError}, get, post, HttpResponse};
use actix_web::web::{Json, Data, ServiceConfig};
use webapp::protocol::request::{CreateApplicantRequest, CreateProfileRequest};
use actix::Addr;
use crate::database::{DatabaseExecutor, CreateProfile};

#[post("/profile")]
async fn create_profile(
    payload: Json<CreateProfileRequest>,
    database: Data<Addr<DatabaseExecutor>>
)-> Result <HttpResponse, Error>{
    let parameters = payload.into_inner();

    let result = database
        .send(CreateProfile {
            applicant_id: parameters.applicant_id,
            recruiter_id: parameters.recruiter_id,
            first_name: parameters.first_name,
            last_name: parameters.last_name,
            date_of_birth: parameters.date_of_birth,
            years_experience: parameters.years_experience,
            profession_id: parameters.profession_id,
            salary: parameters.salary,
            visa_type_id: parameters.visa_type_id,
            work_location_id: parameters.work_location_id,
            country: parameters.country,
            process_step_id: parameters.process_step_id
        })
        .await?;

    let profile_record = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(profile_record))
}

pub fn init_routes(cfg: &mut ServiceConfig){
    cfg.service(create_profile);
}
use actix_web::{error::{Error, ErrorInternalServerError}, get, post, HttpResponse};
use actix_web::web::{Json, Data, ServiceConfig};
use webapp::protocol::request::CreateApplicantRequest;
use actix::Addr;
use crate::database::{DatabaseExecutor, CreateApplicantUser};

#[post("user")]
async fn create_user(
    payload: Json<CreateApplicantRequest>,
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error>{
    let email = payload.into_inner().0;

    let result = database
        .send(CreateApplicantUser(email))
        .await?;

    let user_record = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(user_record))
}

pub fn init_routes(cfg: &mut ServiceConfig) {
    cfg.service(create_user);
}
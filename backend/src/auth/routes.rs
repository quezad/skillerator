use actix_web::{error::{ErrorInternalServerError, Error}, post, get, web::{Data, Json, ServiceConfig}, HttpResponse};
use serde_json::json;
use uuid::Uuid;
use webapp::protocol::{
    request::{
        LoginSession,
        FindUserBySessionTokenRequest
    },
    response::{
        Login,
        User
    }
};
use crate::{
    database::{
        DatabaseExecutor,
        UpdateSession,
        FindUserBySessionToken
    },
};
use actix::prelude::*;

#[post("/who-am-i")]
async fn who_am_i(
    payload: Json<FindUserBySessionTokenRequest>,
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error> {
    let token = payload.into_inner().0;

    let result = database
        .send(FindUserBySessionToken(token))
        .await?;
    let user_info = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(user_info))

}

pub fn init_routes(cfg: &mut ServiceConfig) {
    cfg.service(who_am_i);
}
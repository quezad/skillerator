//! Everything related to database handling

use actix::prelude::*;
use anyhow::Result;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool},
    update
};
use log::debug;
use webapp::{
    protocol::{
        model::{UserSession, Profile, User},
        response::{ProfilesList, ProfileItem}
    },
    schema::user,
    schema::user::dsl::*,
    schema::work_fields::dsl::*,
    schema::countries::dsl::*,
    schema::visa_types::dsl::*,
    schema::work_locations::dsl::*,
    views::detailed_profiles::dsl::*,
    schema::profiles::dsl::*,
};
use uuid::Uuid;
use webapp::protocol::model::{WorkField, Country, VisaType, WorkLocation, ProfileObj};
use chrono::NaiveDate;
use bigdecimal::BigDecimal;


/// The database executor actor
pub struct DatabaseExecutor(pub Pool<ConnectionManager<PgConnection>>);

impl Actor for DatabaseExecutor {
    type Context = SyncContext<Self>;
}

/// The create session message
pub struct CreateSession{
    /// The generated session token
    pub session_token: String,
    // The username to search for
    pub name: String,
    // The password to search for
    pub password: String,
}

impl Message for CreateSession {
    type Result = Result<User>;
}

impl Handler<CreateSession> for DatabaseExecutor {
    type Result = Result<User>;

    fn handle(&mut self, msg: CreateSession, _: &mut Self::Context) -> Self::Result {
        // Insert the session into the database
        debug!("Creating new session for user: {}", msg.name);
        Ok(update(user)
            .filter(email.eq(&msg.name))
            .filter(password.eq(&msg.password))
            .set(session_token.eq(&msg.session_token))
            .get_result::<User>(&self.0.get()?)?)
    }
}

pub struct CreateApplicantUser(pub String);

impl Message for CreateApplicantUser{
    type Result = Result<User>;
}

impl Handler<CreateApplicantUser> for DatabaseExecutor{
    type Result = Result<User>;

    fn handle(&mut self, msg: CreateApplicantUser, _: &mut Self::Context) -> Self::Result{
        debug!("Creating user for {} ", msg.0);
        Ok(
            diesel::insert_into(user)
                .values(email.eq(msg.0))
                .get_result::<User>(&self.0.get()?)?
        )
    }
}

/// The create profile message
pub struct CreateProfile {
    /// The applicant id
    pub applicant_id: Uuid,
    /// The recruiter id
    pub recruiter_id: Uuid,
    /// The applicants first name
    pub first_name: String,
    /// The applicant's last name
    pub last_name: String,
    /// The applicant's date of birth
    pub date_of_birth: NaiveDate,
    /// The applicant's experience
    pub years_experience: BigDecimal,
    /// The profession id
    pub profession_id: i32,
    /// The salary expectations
    pub salary: BigDecimal,
    /// The visa type id
    pub visa_type_id: i32,
    /// The work location id
    pub work_location_id: i32,
    /// The country code
    pub country: String,
    /// The current process step
    pub process_step_id: i32,
}

impl Message for CreateProfile{
    type Result = Result<ProfileObj>;
}

impl Handler<CreateProfile> for DatabaseExecutor{
    type Result = Result<ProfileObj>;

    fn handle(&mut self, msg: CreateProfile, _: &mut Self::Context) -> Self::Result{
        debug!("Creating profile for {} ", msg.first_name);
        Ok(
            diesel::insert_into(profiles)
                .values((
                    applicant_id.eq(msg.applicant_id),
                    recruiter_id.eq(msg.recruiter_id),
                    first_name.eq(msg.first_name),
                    last_name.eq(msg.last_name),
                    date_of_birth.eq(msg.date_of_birth),
                    years_experience.eq(msg.years_experience),
                    profession_id.eq(msg.profession_id),
                    salary.eq(msg.salary),
                    visa_type_id.eq(msg.visa_type_id),
                    work_location_id.eq(msg.work_location_id),
                    country.eq(msg.country),
                    process_step_id.eq(msg.process_step_id)
                ))
                .get_result::<ProfileObj>(&self.0.get()?)?
        )
    }
}

pub struct FindUserBySessionToken(pub String);

impl Message for FindUserBySessionToken {
    type Result = Result<User>;
}

impl Handler<FindUserBySessionToken> for DatabaseExecutor{
    type Result = Result<User>;

    fn handle(&mut self, msg: FindUserBySessionToken, _: &mut Self::Context) -> Self::Result{
        debug!("Validating identity for user: {}", msg.0);
        Ok(
            user.filter(session_token.eq(&msg.0)).get_result::<User>(&self.0.get()?)?
        )
    }
}

pub struct FindAllProfessions;

impl Message for FindAllProfessions {
    type Result = Result<Vec<WorkField>>;
}

impl Handler<FindAllProfessions> for DatabaseExecutor{
    type Result = Result<Vec<WorkField>>;

    fn handle(&mut self, msg: FindAllProfessions, _: &mut Self::Context) -> Self::Result{
        debug!("Getting the work fields collection");
        Ok(
            work_fields.load::<WorkField>(&self.0.get()?)?
        )
    }
}

pub struct FindAllCountries;

impl Message for FindAllCountries {
    type Result = Result<Vec<Country>>;
}

impl Handler<FindAllCountries> for DatabaseExecutor{
    type Result = Result<Vec<Country>>;

    fn handle(&mut self, msg: FindAllCountries, _: &mut Self::Context) -> Self::Result{
        debug!("Getting the countries collection");
        Ok(
            countries.load::<Country>(&self.0.get()?)?
        )
    }
}

pub struct FindAllVisaTypes;

impl Message for FindAllVisaTypes {
    type Result = Result<Vec<VisaType>>;
}

impl Handler<FindAllVisaTypes> for DatabaseExecutor{
    type Result = Result<Vec<VisaType>>;

    fn handle(&mut self, msg: FindAllVisaTypes, _: &mut Self::Context) -> Self::Result{
        debug!("Getting the visa types collection");
        Ok(
            visa_types.load::<VisaType>(&self.0.get()?)?
        )
    }
}

pub struct FindAllWorkLocations;

impl Message for FindAllWorkLocations {
    type Result = Result<Vec<WorkLocation>>;
}

impl Handler<FindAllWorkLocations> for DatabaseExecutor{
    type Result = Result<Vec<WorkLocation>>;

    fn handle(&mut self, msg: FindAllWorkLocations, _: &mut Self::Context) -> Self::Result{
        debug!("Getting the work locations collection");
        Ok(
            work_locations.load::<WorkLocation>(&self.0.get()?)?
        )
    }
}

/// The update session message
pub struct UpdateSession {
    /// The old session token
    pub old_token: String,

    /// The new session token
    pub new_token: String,
}

impl Message for UpdateSession {
    type Result = Result<User>;
}

impl Handler<UpdateSession> for DatabaseExecutor {
    type Result = Result<User>;

    fn handle(&mut self, msg: UpdateSession, _: &mut Self::Context) -> Self::Result {
        // Update the session
        debug!("Updating session: {}", msg.old_token);
        Ok(update(user.filter(session_token.eq(&msg.old_token)))
            .set(session_token.eq(&msg.new_token))
            .get_result::<User>(&self.0.get()?)?)
    }
}

/// The delete session message, needs a token
pub struct DeleteSession(pub String);

impl Message for DeleteSession {
    type Result = Result<()>;
}

impl Handler<DeleteSession> for DatabaseExecutor {
    type Result = Result<()>;

    fn handle(&mut self, msg: DeleteSession, _: &mut Self::Context) -> Self::Result {
        // Delete the session
        debug!("Deleting session: {}", msg.0);
        update(user.filter(session_token.eq(&msg.0)))
            .set(session_token.eq(String::new()))
            .execute(&self.0.get()?)?;
        Ok(())
    }
}

/// The find profiles for user, needs a person id
pub struct FindProfilesForUser(pub Uuid);

impl Message for FindProfilesForUser{
    type Result = Result<Vec<Profile>>;
}

impl Handler<FindProfilesForUser> for DatabaseExecutor {
    type Result = Result<Vec<Profile>>;

    fn handle(&mut self, msg: FindProfilesForUser, _: &mut Self::Context) -> Self::Result {
        debug!("Looking for the list of profiles associated to user: {}", msg.0);
        let hr_responsible = user.find(msg.0).get_result::<User>(&self.0.get()?)?;
        Ok(
            Profile::belonging_to(&hr_responsible).load::<Profile>(&self.0.get()?)?
        )
    }
}

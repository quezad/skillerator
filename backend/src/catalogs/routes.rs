use actix_web::{error::{ErrorInternalServerError, Error}, get, web::{Data, Json, ServiceConfig}, HttpResponse};
use webapp::protocol::model::WorkField;
use crate::database::{DatabaseExecutor, FindAllProfessions, FindAllCountries, FindAllWorkLocations, FindAllVisaTypes};
use futures::SinkExt;
use actix::Addr;

#[get("/professions")]
async fn get_professions(
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error>{
    let result = database
        .send(FindAllProfessions)
        .await?;
    let professions_list = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(professions_list))
}

#[get("/nationalities")]
async fn get_nationalities(
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error>{
    let result = database
        .send(FindAllCountries)
        .await?;
    let countries_list = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(countries_list))
}

#[get("/work_locations")]
async fn get_work_locations(
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error>{
    let result = database
        .send(FindAllWorkLocations)
        .await?;
    let work_locations_list = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(work_locations_list))
}

#[get("/visa_types")]
async fn get_visa_types(
    database: Data<Addr<DatabaseExecutor>>
) -> Result<HttpResponse, Error>{
    let result = database
        .send(FindAllVisaTypes)
        .await?;
    let visa_types_list = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(visa_types_list))
}

pub fn init_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_professions);
    cfg.service(get_nationalities);
    cfg.service(get_work_locations);
    cfg.service(get_visa_types);
}
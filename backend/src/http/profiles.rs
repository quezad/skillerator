//! The get profiles request

use crate::database::{DatabaseExecutor, FindProfilesForUser};
use actix::prelude::*;
use actix_web::{
    web::{Data, Json, Path},
    error::ErrorInternalServerError,
    Error, HttpResponse
};
use log::debug;
use webapp::protocol::{request, response};
use webapp::protocol::request::FindProfilesForHR;
use webapp::protocol::response::ProfilesList;
use uuid::Uuid;

pub async fn find_profiles_for_hr(
    hr_id: Path<Uuid>,
    database: Data<Addr<DatabaseExecutor>>,
) -> Result<HttpResponse, Error> {
    // Remove the session from the database
    debug!("Getting profiles list for HR id {}", &hr_id);

    let result = database.send(FindProfilesForUser(hr_id.into_inner())).await?;

    let profiles_list = result.map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok()
        .json(
            profiles_list
        )
    )
}

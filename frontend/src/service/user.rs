use yew::{callback::Callback, services::fetch::FetchTask};
use crate::api::Requests;
use uuid::Uuid;
use webapp::protocol::response::{ProfilesList, User};
use crate::error::Error;
use crate::types::UserInfo;

/// Apis for user handling
#[derive(Default, Debug)]
pub struct UserService {
    requests: Requests,
}

impl UserService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    pub fn create_new_applicant(
        &mut self,
        email: String,
        callback: Callback<Result<User, Error>>,
    ) -> FetchTask{
        self.requests.post::<String, User>(
            "user".to_string(),
            email,
            callback
        )
    }

    /// Get user identity.
    pub fn who_am_i(
        &mut self,
        token: String,
        callback: Callback<Result<User, Error>>,
    ) -> FetchTask{
        self.requests
            .post::<String, User>(
                "who-am-i".to_string(),
                token
                , callback
            )
    }
}
use yew::{callback::Callback, services::fetch::FetchTask};
use crate::api::Requests;
use uuid::Uuid;
use webapp::protocol::response::ProfilesList;
use webapp::protocol::request::CreateProfileRequest;
use webapp::protocol::model::ProfileObj;
use crate::error::Error;

/// Apis for profile handling
#[derive(Default, Debug)]
pub struct ProfileService {
    requests: Requests,
}

impl ProfileService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    /// Get list of profiles for a give user.
    pub fn by_recruiter_id(
        &mut self,
        recruiter_id: Uuid,
        callback: Callback<Result<ProfilesList, Error>>,
    ) -> FetchTask{
        self.requests
            .get::<ProfilesList>(format!("profiles/{}", recruiter_id), callback)
    }

    pub fn create_profile(
        &mut self,
        profile_info: CreateProfileRequest,
        callback: Callback<Result<ProfileObj, Error>>,
    ) -> FetchTask{
        self.requests
            .post::<CreateProfileRequest, ProfileObj>("profile".to_string(), profile_info, callback)
    }
}

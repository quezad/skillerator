use crate::api::Requests;
use yew::{callback::Callback, services::fetch::FetchTask, callback};
use crate::error::Error;
use webapp::protocol::response::{NationalitiesListResponse, ProfessionsListResponse, VisaTypesListResponse, WorkLocationsListResponse};

/// Api for catalog handling
#[derive(Default, Debug)]
pub struct ProfessionsCatalogService{
    requests: Requests,
}

impl ProfessionsCatalogService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    pub fn get_all(
        &mut self,
        callback: Callback<Result<ProfessionsListResponse, Error>>,
    ) -> FetchTask{
        self.requests.get::<ProfessionsListResponse>(
            "professions".to_string(),
            callback)
    }
}

#[derive(Default, Debug)]
pub struct NationalitiesCatalogService {
    requests: Requests,
}

impl NationalitiesCatalogService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    pub fn get_all(
        &mut self,
        callback: Callback<Result<NationalitiesListResponse, Error>>,
    ) -> FetchTask{
        self.requests.get::<NationalitiesListResponse>(
            "nationalities".to_string(),
            callback)
    }
}

#[derive(Default, Debug)]
pub struct VisaTypeCatalogService {
    requests: Requests,
}

impl VisaTypeCatalogService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    pub fn get_all(
        &mut self,
        callback: Callback<Result<VisaTypesListResponse, Error>>,
    ) -> FetchTask{
        self.requests.get::<VisaTypesListResponse>(
            "visa_types".to_string(),
            callback)
    }
}

#[derive(Default, Debug)]
pub struct WorkLocationCatalogService {
    requests: Requests,
}

impl WorkLocationCatalogService{
    pub fn new() -> Self{
        Self{
            requests: Requests::new(),
        }
    }

    pub fn get_all(
        &mut self,
        callback: Callback<Result<WorkLocationsListResponse, Error>>,
    ) -> FetchTask{
        self.requests.get::<WorkLocationsListResponse>(
            "work_locations".to_string(),
            callback)
    }
}


use yew::ComponentLink;
use yew::{agent::Bridged, format::Json, html, prelude::*, services::fetch::FetchTask};
use yew_router::agent::RouteAgent;
use log::{error, info, warn};
use crate::{
    api::Response,
    route::RouterTarget,
    service::{
        cookie::CookieService,
        session_timer::{self, SessionTimerAgent},
        uikit::{NotificationStatus, UIkitService},
    },
    string::{REQUEST_ERROR, RESPONSE_ERROR, TEXT_CONTENT},
    SESSION_COOKIE,
};
use webapp::{
    protocol::{model::UserSession, request, response},
    API_URL_LOGOUT,
};

use yew_router::agent::RouteRequest::ChangeRoute;


/// The summary page

pub struct SummaryComponent{
    component_link: ComponentLink<SummaryComponent>,
    cookie_service: CookieService,
    fetch_task: Option<FetchTask>,
    logout_button_disabled: bool,
    router_agent: Box<dyn Bridge<RouteAgent<()>>>,
    session_timer_agent: Box<dyn Bridge<SessionTimerAgent>>,
    uikit_service: UIkitService,
}

pub enum Message{
    Fetch(Response<response::Logout>),
    Ignore,
    LogoutRequest,
    InviteRequest,
}

impl Component for SummaryComponent{
    type Message = Message;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        // Guard the authentication
        let mut router_agent = RouteAgent::bridge(link.callback(|_| Message::Ignore));
        let cookie_service = CookieService::new();
        let mut session_timer_agent = SessionTimerAgent::bridge(link.callback(|_| Message::Ignore));
        if cookie_service.get(SESSION_COOKIE).is_err() {
            info!("No session token found, routing back to login");
            router_agent.send(ChangeRoute(RouterTarget::Login.into()));
        } else {
            // Start the timer to keep the session active
            session_timer_agent.send(session_timer::Request::Start);
        }

        // Return the component
        Self {
            component_link: link,
            cookie_service,
            fetch_task: None,
            logout_button_disabled: false,
            router_agent,
            session_timer_agent,
            uikit_service: UIkitService::new(),
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    /// Called everytime when messages are received
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Message::InviteRequest => {
                self.router_agent
                    .send(ChangeRoute(RouterTarget::Content.into()));
            }
            Message::LogoutRequest => {
                if let Ok(token) = self.cookie_service.get(SESSION_COOKIE) {
                    self.fetch_task = fetch! {
                        request::LogoutSession(token) => API_URL_LOGOUT,
                        self.component_link, Message::Fetch,
                        || {
                            // Disable user interaction
                            self.logout_button_disabled = true;
                        },
                        || {
                            error!("Unable to create logout request");
                            self.uikit_service
                                .notify(REQUEST_ERROR, &NotificationStatus::Danger);
                        }
                    };
                } else {
                    // It should not happen but in case there is no session cookie on logout, route
                    // back to login
                    error!("No session cookie found");
                    self.router_agent
                        .send(ChangeRoute(RouterTarget::Login.into()));
                }
            }

            // The message for all fetch responses
            Message::Fetch(response) => {
                let (meta, Json(body)) = response.into_parts();

                // Check the response type
                if meta.status.is_success() {
                    match body {
                        Ok(response::Logout) => info!("Got valid logout response"),
                        _ => {
                            warn!("Got wrong logout response");
                            self.uikit_service
                                .notify(RESPONSE_ERROR, &NotificationStatus::Danger);
                        }
                    }
                } else {
                    warn!("Logout failed with status: {}", meta.status);
                }

                // Remove the existing cookie
                self.cookie_service.remove(SESSION_COOKIE);
                self.session_timer_agent.send(session_timer::Request::Stop);
                self.router_agent
                    .send(ChangeRoute(RouterTarget::Login.into()));
                self.logout_button_disabled = true;

                // Remove the ongoing task
                self.fetch_task = None;
            }
            Message::Ignore => {}
        }
        true
    }

    fn view(&self) -> Html {
        let onclick = self.component_link.callback(|_| Message::LogoutRequest);
        let onclick_invite = self.component_link.callback(|_| Message::InviteRequest);
        html! {
            <div class="uk-card uk-card-body">
                <div class="uk-card-header">
                    <div class="uk-grid uk-flex-left">
                        <img src="images/logo_skillerator.svg" alt="" width="173" height="77"/>
                    </div>
                </div>
                <div class="uk-card-body">
                    <div class="uk-grid uk-flex-left">
                        <div class="uk-width-auto">
                            <ul class="uk-iconnav uk-iconnav-vertical">
                                <li><a href="#" uk-icon="icon: home; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: cog; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: bell; ratio: 2"></a></li>
                                <li><a onclick=onclick uk-icon="icon: sign-out; ratio: 2",
                                ></a></li>
                            </ul>
                        </div>
                        <div class="uk-width-expand">
                            <div class="uk-grid-small uk-grid-match uk-width-1-1 uk-child-width-1-6 uk-text-center uk-grid">
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: user;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Create Profile"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                </div>
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: user;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Schedule Appointment at Embassy"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                </div>
                                <div>
                                </div>
                                <div>
                                </div>
                            </div>
                            <div class="uk-grid-small uk-grid-match uk-width-1-1 uk-child-width-1-6 uk-text-center uk-grid">
                                <div>
                                    <hr class="uk-divider-icon" style="background-color:#F18739"/>
                                </div>
                                <div>
                                    <hr class="uk-divider-icon"/>
                                </div>
                                <div>
                                    <hr class="uk-divider-icon"/>
                                </div>
                                <div>
                                    <hr class="uk-divider-icon"/>
                                </div>
                                <div>
                                    <hr class="uk-divider-icon"/>
                                </div>
                                <div>
                                    <hr class="uk-divider-icon"/>
                                </div>
                            </div>
                            <div class="uk-grid-small uk-grid-match uk-width-1-1 uk-child-width-1-6 uk-text-center uk-grid">
                                <div>
                                </div>
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: bolt;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Send Power of Attorney"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                </div>
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: bolt;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Apply for Visa"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: bolt;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Journey to Country"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="uk-card uk-card-default uk-card-body">
                                        <div class="uk-card-badge">
                                            <span class="uk-icon-button uk-active" uk-icon="icon: bolt;">
                                            </span>
                                        </div>
                                        <div class="uk-card-body">
                                            <p class="uk-text-small">{"Apply for Work Permit"}</p>
                                        </div>
                                        <div class="uk-card-footer">
                                            <a class="uk-button uk-button-text">{" See More..."}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-grid-small uk-width-1-1 uk-child-width-1-3 uk-text-center uk-grid">
                                <div>
                                </div>
                                <div style="padding-top: 30px">
                                    <button class="uk-button uk-button-primary",
                                    type="submit",
                                    onclick=onclick_invite>{"Invite Applicant"}</button>
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-card-footer">

                </div>

            </div>
        }
    }
}
use crate::service::user::UserService;
use webapp::protocol::request::{CreateApplicantRequest, CreateProfileRequest};
use yew::{
    services::fetch::FetchTask,
    Callback,
    Component,
    html,
    prelude::*,
    ComponentLink,
    Bridge,
    ShouldRender,
    Html,
    Bridged
};
use crate::error::Error;
use webapp::protocol::response::User;
use crate::service::uikit::UIkitService;
use chrono::NaiveDate;
use crate::api::Response;
use yew_router::agent::{RouteAgent, RouteRequest::ChangeRoute};
use crate::route::RouterTarget;

pub struct ProfilePersonalInfoChildComponent{
    fetch_task: Option<FetchTask>,
    component_link: ComponentLink<ProfilePersonalInfoChildComponent>,
    user_service: UserService,
    user_data_request: Option<CreateApplicantRequest>,
    user_creation_response: Callback<Result<User, Error>>,
    router_agent: Box<dyn Bridge<RouteAgent>>,
    uikit_service: UIkitService,
    first_name_field: String,
    last_name_field: String,
    email_field: String,
    dob_field: String,
    inputs_disabled: bool,
    quals_button_disabled: bool,
    props: Props,
}

pub enum Message{
    Fetch(Response<User>),
    QualificationsRequest,
    CreateApplicantUser(Result<User, Error>),
    Ignore,
    UpdateFirstNameField(String),
    UpdateLastNameField(String),
    UpdateEmailField(String),
    UpdateDoBField(String),
}

#[derive(Properties, Clone)]
pub struct Props {
    pub step: i32,
    pub profile_details: CreateProfileRequest,
}

impl Component for ProfilePersonalInfoChildComponent{
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut router_agent= RouteAgent::bridge(link.callback(|_| Message::Ignore));

        Self{
            fetch_task: None,
            component_link: link.clone(),
            user_service: UserService::new(),
            user_data_request: None,
            user_creation_response: link.callback(Message::CreateApplicantUser),
            router_agent,
            uikit_service: UIkitService::new(),
            first_name_field: String::new(),
            last_name_field: String::new(),
            email_field: String::new(),
            dob_field: String::new(),
            inputs_disabled: false,
            quals_button_disabled: true,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender{
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg{
            Message::CreateApplicantUser(Ok(user_info)) => {
                self.quals_button_disabled = true;
                self.inputs_disabled = true;
                self.props.step = 2;
                self.router_agent.send(ChangeRoute(RouterTarget::ProfileCreation.into()));
                self.fetch_task = None;
            }
            Message::CreateApplicantUser(Err(_)) => {

            }
            Message::QualificationsRequest =>{
                self.fetch_task = Some(
                    self.user_service.create_new_applicant(
                        self.email_field.clone(),
                        self.user_creation_response.clone()
                    )
                );
            }
            Message::Ignore=> {}
            Message::UpdateFirstNameField(new_first_name) => {
                self.first_name_field = new_first_name.clone();
                self.props.profile_details.first_name = new_first_name;
                self.update_button_state();
            }
            Message::UpdateLastNameField(new_last_name) => {
                self.last_name_field = new_last_name.clone();
                self.props.profile_details.first_name = new_last_name;
                self.update_button_state();
            }
            Message::UpdateEmailField(new_email) => {
                self.email_field = new_email.clone();
                self.update_button_state();
            }
            Message::UpdateDoBField(new_dob) => {
                self.dob_field = new_dob.clone();
                if let Ok(parsed_dob) = NaiveDate::parse_from_str(&new_dob, "%d.%m.%y") {
                    self.props.profile_details.date_of_birth = parsed_dob;
                    self.update_button_state();
                } else{

                }
            }
            _ => {}
        }
        true
    }

    fn view(&self) -> Html{
        let onclick =
            self.component_link.callback(|_| Message::QualificationsRequest);

        let oninput_first_name = self
            .component_link
            .callback(|e: InputData| Message::UpdateFirstNameField(e.value));
        let oninput_last_name = self
            .component_link
            .callback(|e: InputData| Message::UpdateLastNameField(e.value));
        let oninput_email = self
            .component_link
            .callback(|e: InputData| Message::UpdateEmailField(e.value));
        let oninput_dob = self
            .component_link
            .callback(|e: InputData| Message::UpdateDoBField(e.value));

        html!{
            <form class="uk-grid-small">
                <fieldset class="uk-fieldset",>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"First Name"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                            id="form-stacked-text",
                            type="text",
                            value=&self.first_name_field,
                            oninput=oninput_first_name
                            placeholder={"First Name..."}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Last Name"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                             id="form-stacked-text",
                             type="text",
                             value=&self.last_name_field,
                             oninput=oninput_last_name,
                             placeholder={"Last Name..."}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"E-mail"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                             id="form-stacked-text",
                             type="text",
                             value=&self.email_field,
                             oninput=oninput_email,
                             placeholder={"E-mail Address..."}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Date of Birth"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                            id="form-stacked-text",
                            type="text",
                            value=&self.dob_field,
                            oninput=oninput_dob,
                            placeholder={"dd.mm.yyy"}/>
                        </div>
                    </div>
                    <button class="uk-button uk-button-primary",
                        type="submit",
                        disabled=self.quals_button_disabled,
                        onclick=onclick>{"Qualifications"}</button>
                </fieldset>
            </form>
        }
    }
}

impl ProfilePersonalInfoChildComponent {
    fn update_button_state(&mut self) {
        self.quals_button_disabled = self.first_name_field.is_empty() ||
            self.last_name_field.is_empty() ||
            self.dob_field.is_empty() ||
            self.email_field.is_empty();
    }
}
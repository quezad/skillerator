use crate::error::Error;
use crate::service::catalogs::{
    VisaTypeCatalogService
};
use yew::{Callback, Component, ComponentLink, ShouldRender, services::fetch::FetchTask, html, Html, Properties, ChangeData};
use webapp::protocol::response::ProfilesList;
use webapp::protocol::response::VisaTypesListResponse;
use crate::api::Response;


/// Data Model for the Professions dropdown
pub struct VisaTypeOptionsList {
    visa_types_service: VisaTypeCatalogService,
    visa_types_service_response: Callback<Result<VisaTypesListResponse, Error>>,
    visa_types_list: Option<VisaTypesListResponse>,
    fetch_task: Option<FetchTask>,
    props: Props,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub onchange: Callback<ChangeData>,
}

/// Available message types to process
pub enum Message{
    LoadVisaTypesList(Result<VisaTypesListResponse, Error>),
}

impl Component for VisaTypeOptionsList {
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self{
        Self{
            visa_types_service: VisaTypeCatalogService::new(),
            visa_types_service_response: link.callback(Message::LoadVisaTypesList),
            visa_types_list: None,
            fetch_task: None,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        self.fetch_task = Some(self.visa_types_service.get_all(self.visa_types_service_response.clone()));
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender{ false }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg {
            Message::LoadVisaTypesList(Ok(visa_types_list)) => {
                self.visa_types_list = Some(visa_types_list);
            }
            Message::LoadVisaTypesList(Err(_)) =>{
                self.visa_types_list = None;
            }
        }
        true
    }

    fn view(&self) -> Html{
        html! {
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">{"Visa Type"}</label>
                <select class="uk-select",
                onchange=&self.props.onchange>
                {
                    if let Some(visa_types_list) = &self.visa_types_list{
                        html!{
                            <>
                                {for visa_types_list.0.iter().map(|option|{
                                    html!{
                                    <>
                                        <option value=&option.id>{&option.description}</option>
                                    </>
                                    }
                                })}
                            </>
                        }
                    } else {
                        html! {
                            <>
                                <option value={-1}>{"No options here ..."}</option>
                            </>
                        }
                    }
                }
                </select>
            </div>
        }
    }
}
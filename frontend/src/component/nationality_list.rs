use crate::error::Error;
use crate::service::catalogs::NationalitiesCatalogService;
use yew::{Callback, Component, ComponentLink, ShouldRender, services::fetch::FetchTask, html, Html, Properties, ChangeData};
use webapp::protocol::response::{NationalitiesListResponse, NationalityOption};
use crate::api::Response;
use yew::components::Select;
use serde::de::Unexpected::Enum;


/// Data Model for the Professions dropdown
pub struct NationalityOptionsList {
    nationalities_service: NationalitiesCatalogService,
    nationality_service_response: Callback<Result<NationalitiesListResponse, Error>>,
    nationalities_list: Option<NationalitiesListResponse>,
    fetch_task: Option<FetchTask>,
    props: Props,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub onchange: Callback<ChangeData>,
}

/// Available message types to process
pub enum Message{
    LoadNationalitiesList(Result<NationalitiesListResponse, Error>),
}

impl Component for NationalityOptionsList {
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self{
        Self{
            nationalities_service: NationalitiesCatalogService::new(),
            nationality_service_response: link.callback(Message::LoadNationalitiesList),
            nationalities_list: None,
            fetch_task: None,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        self.fetch_task = Some(self.nationalities_service.get_all(self.nationality_service_response.clone()));
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender{ false }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg {
            Message::LoadNationalitiesList(Ok(nationalities_list)) => {
                self.nationalities_list = Some(nationalities_list);
            }
            Message::LoadNationalitiesList(Err(_)) =>{
                self.nationalities_list = None;
            }
        }
        true
    }

    fn view(&self) -> Html{
        html! {
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">{"Nationality"}</label>
                <select class="uk-select",
                onchange=&self.props.onchange>
                {
                    if let Some(nationalities_list) = &self.nationalities_list{
                        html!{
                            <>
                                {for nationalities_list.0.iter().map(|option|{
                                    html!{
                                    <>
                                        <option value=&option.id>{&option.description}</option>
                                    </>
                                    }
                                })}
                            </>
                        }
                    } else {
                        html! {
                            <>
                                <option value={-1}>{"No options here ..."}</option>
                            </>
                        }
                    }
                }
                </select>
            </div>
        }
    }
}
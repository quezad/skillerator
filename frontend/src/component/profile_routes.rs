//! Available routes within profile creation

use yew_router::prelude::*;

#[derive(Clone, Switch)]
pub enum RouterTarget {
    #[to = "/#create_profile/personal_info"]
    PersonalInfoChildComponent,

    #[to = "/#create_profile/qualifications"]
    QualificationsChildComponent,

    #[to = "/#create_profile/employment_info"]
    EmploymentInfoChildComponent,
}

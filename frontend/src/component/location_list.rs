use crate::error::Error;
use crate::service::catalogs::WorkLocationCatalogService;
use yew::{Callback, Component, ComponentLink, ShouldRender, services::fetch::FetchTask, html, Html, Properties, ChangeData};
use webapp::protocol::response::{WorkLocationsListResponse, WorkLocationOption};
use crate::api::Response;
use yew::components::Select;
use serde::de::Unexpected::Enum;


/// Data Model for the Professions dropdown
pub struct WorkLocationOptionsList {
    work_locations_service: WorkLocationCatalogService,
    work_location_service_response: Callback<Result<WorkLocationsListResponse, Error>>,
    work_locations_list: Option<WorkLocationsListResponse>,
    fetch_task: Option<FetchTask>,
    props: Props,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub onchange: Callback<ChangeData>,
}

/// Available message types to process
pub enum Message{
    LoadWorkLocationsList(Result<WorkLocationsListResponse, Error>),
}

impl Component for WorkLocationOptionsList {
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self{
        Self{
            work_locations_service: WorkLocationCatalogService::new(),
            work_location_service_response: link.callback(Message::LoadWorkLocationsList),
            work_locations_list: None,
            fetch_task: None,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        self.fetch_task = Some(self.work_locations_service.get_all(self.work_location_service_response.clone()));
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender{ false }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg {
            Message::LoadWorkLocationsList(Ok(work_locations_list)) => {
                self.work_locations_list = Some(work_locations_list);
            }
            Message::LoadWorkLocationsList(Err(_)) =>{
                self.work_locations_list = None;
            }
        }
        true
    }

    fn view(&self) -> Html{
        html! {
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">{"Work Location"}</label>
                <select class="uk-select",
                onchange=&self.props.onchange>
                {
                    if let Some(work_locations_list) = &self.work_locations_list{
                        html!{
                            <>
                                {for work_locations_list.0.iter().map(|option|{
                                    html!{
                                    <>
                                        <option value=&option.id>{&option.description}</option>
                                    </>
                                    }
                                })}
                            </>
                        }
                    } else {
                        html! {
                            <>
                                <option value={-1}>{"No options here ..."}</option>
                            </>
                        }
                    }
                }
                </select>
            </div>
        }
    }
}
//! All available components of this application

mod content;
mod login;
mod landing_hr;
mod applicant_card;
mod profile_details;
mod professions_list;
mod nationality_list;
mod visa_list;
mod location_list;
mod summary;
pub mod root;

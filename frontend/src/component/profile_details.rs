//! The Profile Details Section

use yew::{ComponentLink, Bridge, Component, prelude::*, html, ShouldRender, services::fetch::FetchTask};
use crate::service::cookie::CookieService;
use crate::service::session_timer::{self, SessionTimerAgent};
use yew_router::agent::RouteAgent;
use crate::service::uikit::{UIkitService, NotificationStatus};
use webapp::protocol::request::{CreateProfileRequest, CreateApplicantRequest};
use webapp::protocol::{
    response,
    request,
    response::{User}
};
use crate::api::Response;
use webapp::protocol::model::{Profile, ProfileObj};
use crate::SESSION_COOKIE;
use yew_router::agent::RouteRequest::ChangeRoute;
use crate::route::RouterTarget;
use log::{error, info, warn};
use crate::service::user::UserService;
use crate::error::Error;
use yew::format::Json;
use webapp::{
    API_URL_LOGOUT
};
use crate::string::{
    RESPONSE_ERROR,
    REQUEST_ERROR,
};

use chrono::NaiveDate;
use crate::component::{
    professions_list::ProfessionOptionsList,
    nationality_list::NationalityOptionsList,
    visa_list::VisaTypeOptionsList,
    location_list::WorkLocationOptionsList,
};
use stdweb::web::html_element::SelectElement;
use yew::ChangeData::Select;
use stdweb::web::event::SelectionChangeEvent;
use stdweb::web::Selection;
use webapp::protocol::response::NationalityOption;
use bigdecimal::BigDecimal;
use std::str::FromStr;
use crate::service::profile::ProfileService;

/// Data Model for the Profile Creation Component
pub struct ProfileCreationComponent {
    user_creation_response: Callback<Result<User, Error>>,
    profile_creation_response: Callback<Result<ProfileObj, Error>>,
    first_name_field: String,
    last_name_field: String,
    email_field: String,
    dob_field: String,
    current_step: i32,
    applicant_profile_info: CreateProfileRequest,
    user_service: UserService,
    user_data: Option<response::User>,
    user_response: Callback<Result<User, Error>>,
    component_link: ComponentLink<ProfileCreationComponent>,
    cookie_service: CookieService,
    fetch_task: Option<FetchTask>,
    inputs_disabled: bool,
    qualifications_button_disabled: bool,
    employment_details_button_disabled: bool,
    confirm_button_disabled: bool,
    save_button_disabled: bool,
    router_agent: Box<dyn Bridge<RouteAgent<()>>>,
    session_timer_agent: Box<dyn Bridge<SessionTimerAgent>>,
    uikit_service: UIkitService,
    profile_details: CreateProfileRequest,
    on_germany_before_field: bool,
    show_info_card: bool,
    show_nationality_info_text: bool,
    show_age_info_text: bool,
    show_previous_german_info_text: bool,
    experience_field: String,
    profession_field: String,
    show_experience_info_text: bool,
    show_profession_info_text: bool,
    show_education_info_text: bool,
    salary_field: String,
    visa_type_field: String,
    work_location_field: String,
    show_salary_info_text: bool,
    show_visa_type_info_text: bool,
    show_work_location_info_text: bool,
    profile_service: ProfileService,
}


/// Available message types to process
pub enum Message {
    Fetch(Response<response::Logout>),
    CreateProfile(Result<ProfileObj, Error>),
    CreateApplicantUser(Result<User, Error>),
    Ignore,
    UpdateUsername(String),
    LoadUserInfo(Result<User, Error>),
    UpdateFirstNameField(String),
    UpdateLastNameField(String),
    UpdateEmailField(String),
    UpdateDoBField(String),
    LogoutRequest,
    QualificationsRequest,
    DisplayNationalityInfoText(bool),
    UpdateNationalityField(ChangeData),
    EmploymentDetailsRequest,
    UpdatePreviouslyOnGermany,
    UpdateEducationField,
    UpdateProfessionField(ChangeData),
    UpdateExperienceField(String),
    UpdateSalaryField(String),
    UpdateVisaTypeField(ChangeData),
    UpdateWorkLocationField(ChangeData),
    SummaryRequest,
}

impl Component for ProfileCreationComponent{
    type Message = Message;
    type Properties = ();

    /// Initialization routine
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        // Guard the authentication
        let mut router_agent = RouteAgent::bridge(link.callback(|_| Message::Ignore));
        let cookie_service = CookieService::new();
        let mut session_timer_agent = SessionTimerAgent::bridge(link.callback(|_| Message::Ignore));
        if cookie_service.get(SESSION_COOKIE).is_err() {
            info!("No session token found, routing back to login");
            router_agent.send(ChangeRoute(RouterTarget::Login.into()));
        } else {
            // Start the timer to keep the session active
            session_timer_agent.send(session_timer::Request::Start);
        }

        Self{
            user_creation_response: link.callback(Message::CreateApplicantUser),
            profile_creation_response: link.callback(Message::CreateProfile),
            first_name_field: String::new(),
            last_name_field: String::new(),
            email_field: String::new(),
            dob_field: String::new(),
            current_step: 1,
            applicant_profile_info: CreateProfileRequest::default(),
            user_service: UserService::new(),
            user_data: None,
            user_response: link.callback(Message::LoadUserInfo),
            cookie_service: CookieService::new(),
            fetch_task: None,
            inputs_disabled: false,
            qualifications_button_disabled: true,
            employment_details_button_disabled: true,
            confirm_button_disabled: true,
            save_button_disabled: true,
            router_agent,
            component_link: link,
            session_timer_agent,
            uikit_service: UIkitService::new(),
            profile_details: CreateProfileRequest::default(),
            show_nationality_info_text: false,
            show_age_info_text: false,
            on_germany_before_field: false,
            show_info_card: false,
            show_previous_german_info_text: false,
            experience_field: String::new(),
            show_experience_info_text: false,
            profession_field: String::new(),
            show_profession_info_text: false,
            show_education_info_text: false,
            salary_field: String::new(),
            visa_type_field: String::new(),
            work_location_field: String::new(),
            show_salary_info_text: false,
            show_visa_type_info_text: false,
            show_work_location_info_text: false,
            profile_service: ProfileService::new(),
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        if let Ok(token) = self.cookie_service.get(SESSION_COOKIE) {
            self.fetch_task = Some(self.user_service.who_am_i(token, self.user_response.clone()));
        }else {
            // It should not happen but in case there is no session cookie on logout, route
            // back to login
            error!("No session cookie found");
            self.router_agent
                .send(ChangeRoute(RouterTarget::Login.into()));
        }
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    /// Called everytime when messages are received
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Message::LogoutRequest => {
                if let Ok(token) = self.cookie_service.get(SESSION_COOKIE) {
                    self.fetch_task = fetch! {
                        request::LogoutSession(token) => API_URL_LOGOUT,
                        self.component_link, Message::Fetch,
                        || {
                            // Disable user interaction
                        },
                        || {
                            error!("Unable to create logout request");
                            self.uikit_service
                                .notify(REQUEST_ERROR, &NotificationStatus::Danger);
                        }
                    };
                } else {
                    // It should not happen but in case there is no session cookie on logout, route
                    // back to login
                    error!("No session cookie found");
                    self.router_agent
                        .send(ChangeRoute(RouterTarget::Login.into()));
                }
            }

            Message::LoadUserInfo(Ok(user_info)) => {
                self.user_data = Some(user_info);
                self.fetch_task = None;
            }
            Message::LoadUserInfo(Err(_)) => {
                self.fetch_task = None;
            }

            Message::CreateProfile(Ok(profile_info)) => {
                self.router_agent
                    .send(ChangeRoute(RouterTarget::SummaryComponent.into()));
                self.fetch_task = None;
            }
            Message::CreateProfile(Err(_)) => {
                self.fetch_task = None;
            }

            Message::CreateApplicantUser(Ok(applicant_info)) => {
                self.profile_details.applicant_id = applicant_info.id;
                self.profile_details.recruiter_id = self.user_data.as_ref().unwrap().id;
                self.qualifications_button_disabled = true;
                self.inputs_disabled = true;
                self.current_step = 2;
                self.fetch_task = None;
            }
            Message::EmploymentDetailsRequest => {
                self.employment_details_button_disabled = true;
                self.inputs_disabled = true;
                self.current_step = 3;

                self.show_previous_german_info_text = false;
                self.show_age_info_text = false;
                self.show_nationality_info_text = false;
                self.show_profession_info_text = false;
                self.show_education_info_text = false;
                self.show_experience_info_text = false;
                self.show_info_card = false;

                self.fetch_task = None;
            }

            Message::CreateApplicantUser(Err(_)) => {
                self.fetch_task = None;
            }

            Message::UpdateUsername(String) => {
                self.fetch_task = None;
            }

            // The message for all fetch responses
            Message::Fetch(response) => {
                let (meta, Json(body)) = response.into_parts();

                // Check the response type
                if meta.status.is_success() {
                    match body {
                        Ok(response::Logout) => info!("Got valid logout response"),
                        _ => {
                            warn!("Got wrong logout response");
                            self.uikit_service
                                .notify(RESPONSE_ERROR, &NotificationStatus::Danger);
                        }
                    }
                } else {
                    warn!("Logout failed with status: {}", meta.status);
                }

                // Remove the existing cookie
                self.cookie_service.remove(SESSION_COOKIE);
                self.session_timer_agent.send(session_timer::Request::Stop);
                self.router_agent
                    .send(ChangeRoute(RouterTarget::Login.into()));

                // Remove the ongoing task
                self.fetch_task = None;
            }
            Message::UpdateFirstNameField(new_first_name) => {
                self.first_name_field = new_first_name.clone();
                self.profile_details.first_name = new_first_name;
                self.update_personal_info_button_state();
            }
            Message::UpdateLastNameField(new_last_name) => {
                self.last_name_field = new_last_name.clone();
                self.profile_details.last_name = new_last_name;
                self.update_personal_info_button_state();
            }
            Message::UpdateEmailField(new_email) => {
                self.email_field = new_email.clone();
                self.update_personal_info_button_state();
            }
            Message::UpdateExperienceField(new_experience) =>{
                self.show_info_card = true;
                self.show_experience_info_text = true;
                self.experience_field = new_experience.clone();
                if let Ok(parsed_experience) = BigDecimal::from_str(&new_experience){
                    self.profile_details.years_experience = parsed_experience;
                    self.update_qualifications_button_state();
                }else{

                }
            }
            Message::UpdateSalaryField(new_salary) =>{
                self.show_info_card = true;
                self.show_salary_info_text = true;
                self.salary_field = new_salary.clone();
                if let Ok(parsed_salary) = BigDecimal::from_str(&new_salary){
                    self.profile_details.salary = parsed_salary;
                    self.update_employment_info_button_state();
                }
            }
            Message::UpdateDoBField(new_dob) => {
                self.show_info_card = true;
                self.show_age_info_text = true;
                self.dob_field = new_dob.clone();
                let to_parse_dob = format!("{}.{}.{}","01","01",&new_dob);
                if let Ok(parsed_dob) = NaiveDate::parse_from_str(&to_parse_dob, "%d.%m.%y") {
                    self.profile_details.date_of_birth = parsed_dob;
                    self.update_personal_info_button_state();
                } else{

                }
            }
            Message::UpdateNationalityField(new_nationality) => {
                if let Select(element) = new_nationality{
                    let nationality_value = element.value();
                    self.show_info_card = true;
                    self.show_nationality_info_text = true;
                    self.profile_details.country=nationality_value.unwrap();
                }else {  }

            }
            Message::UpdateEducationField =>{
                self.show_info_card = true;
                self.show_education_info_text = true;
            }
            Message::UpdateProfessionField(new_profession) =>{
                if let Select(element) = new_profession{
                    let profession_value = element.value();
                    self.show_info_card = true;
                    self.show_profession_info_text = true;
                    self.profile_details.profession_id = profession_value
                        .unwrap()
                        .parse::<i32>()
                        .unwrap();
                }
            }

            Message::UpdateVisaTypeField(new_visa_type) =>{
                if let Select(element) = new_visa_type{
                    let visa_type_value = element.value();
                    self.show_info_card = true;
                    self.show_visa_type_info_text = true;
                    self.profile_details.visa_type_id = visa_type_value
                        .unwrap()
                        .parse::<i32>()
                        .unwrap();
                }
            }

            Message::UpdateWorkLocationField(new_work_location) =>{
                if let Select(element) = new_work_location{
                    let work_location_value = element.value();
                    self.show_info_card = true;
                    self.show_work_location_info_text = true;
                    self.profile_details.work_location_id = work_location_value
                        .unwrap()
                        .parse::<i32>()
                        .unwrap();
                }
            }

            Message::DisplayNationalityInfoText(show_info_text) => {

            }
            Message::UpdatePreviouslyOnGermany => {
                self.on_germany_before_field ^= true;
                self.show_info_card = true;
                self.show_previous_german_info_text = true;
            }
            Message::QualificationsRequest =>{
                self.fetch_task = Some(
                    self.user_service.create_new_applicant(
                        self.email_field.clone(),
                        self.user_creation_response.clone()
                    )
                );
                self.show_previous_german_info_text = false;
                self.show_age_info_text = false;
                self.show_nationality_info_text = false;
                self.show_info_card = false;
            }
            Message::SummaryRequest =>{
                self.profile_details.process_step_id = 3;
                self.fetch_task = Some(
                    self.profile_service.create_profile(
                        self.profile_details.clone(),
                        self.profile_creation_response.clone()
                    )
                );

                self.employment_details_button_disabled = true;
                self.inputs_disabled = true;
                self.current_step = 3;

                self.show_work_location_info_text = false;
                self.show_visa_type_info_text = false;
                self.show_salary_info_text = false;
                self.show_previous_german_info_text = false;
                self.show_age_info_text = false;
                self.show_nationality_info_text = false;
                self.show_profession_info_text = false;
                self.show_education_info_text = false;
                self.show_experience_info_text = false;
                self.show_info_card = false;
            }
            Message::Ignore => {}
        }
        true
    }

    fn view(&self) -> Html {
        let logout = self.component_link.callback(|_| Message::LogoutRequest);
        let mut step_style = ["margin-right:0.5em","margin-right:0.5em","margin-right:0.5em"];
        for i in 0..*&self.current_step as usize{
            step_style[i] = "background:#F18739 ;margin-right:0.5em";
        }

        html! {
            <div class="uk-section uk">
                <div class="uk-card-header">
                    <div class="uk-grid uk-flex-left">
                        <img src="images/logo_skillerator.svg" alt="" width="173" height="77"/>
                    </div>
                </div>
                <div class="uk-card-body">
                    <div class="uk-grid uk-flex-left">
                        <div class="uk-width-auto">
                            <ul class="uk-iconnav uk-iconnav-vertical uk-list-large">
                                <li><a href="#" uk-icon="icon: home; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: cog; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: bell; ratio: 2"></a></li>
                                <li><a onclick=logout uk-icon="icon: sign-out; ratio: 2",
                                ></a></li>
                            </ul>
                        </div>
                        <div class="uk-width-expand">
                            <div class="uk-grid-match uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-grid">
                                <div class="uk-text-center">
                                    <p>
                                        <span class="uk-icon-button uk-active" style=step_style[0] uk-icon="icon: file-text;"></span>
                                        {"Personal Data"}
                                    </p>
                                </div>
                                <div class="uk-text-center">
                                    <p>
                                        <span class="uk-icon-button" style=step_style[1] uk-icon="icon: file-text;"></span>
                                        {"Qualifications"}
                                    </p>
                                </div>
                                <div class="uk-text-center">
                                    <p>
                                        <span class="uk-icon-button" style=step_style[2] uk-icon="icon: folder;"></span>
                                        {"Employment Details"}
                                    </p>
                                </div>
                            </div>
                            <hr/>
                            <div class="uk-grid-match uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-grid">
                                <div>
                                    {
                                    match &self.current_step{
                                        1 => {self.personal_info()},
                                        2 => {self.qualifications()},
                                        3 => {self.employment_details()},
                                        _ => html! { "No child component available" },
                                        }
                                    }
                                </div>
                                <div>
                                    {if self.show_info_card{
                                        html!{
                                            <div class="uk-card uk-card-default uk-card-body uk-width-expand">
                                                <div class="uk-card-badge">
                                                    <span class="uk-icon-button uk-active" style="background:#F18739 ;margin-right:0.5em" uk-icon="icon: info;"></span>
                                                </div>
                                                <h3 class="uk-card-title">{"Considerations"}</h3>
                                                {if self.show_nationality_info_text{
                                                    {self.show_nationality_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_age_info_text{
                                                    {self.show_age_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_previous_german_info_text{
                                                    {self.show_previous_german_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_education_info_text{
                                                    {self.show_education_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_profession_info_text{
                                                    {self.show_profession_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_experience_info_text{
                                                    {self.show_experience_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_salary_info_text{
                                                    {self.show_salary_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_visa_type_info_text{
                                                    {self.show_visa_type_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                                {if self.show_work_location_info_text{
                                                    {self.show_work_location_info_text()}
                                                } else {
                                                    html!{}
                                                }}
                                            </div>
                                        }
                                    }else{
                                        html!{
                                        <div class="uk-card uk-card-body uk-width-expand uk-text-center@m uk-card-body uk-text-italic uk-text-middle uk-text-muted">
                                            {"- If necessary, help and additional information will display here -"}
                                        </div>
                                        }
                                    }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-card-footer">

                </div>

            </div>
        }
    }
}

impl ProfileCreationComponent{
    fn personal_info(&self) -> Html{
        let onclick = self.component_link.callback(|ev:ClickEvent| Message::QualificationsRequest);

        //let nationality_onfocus_callback = self
        //    .component_link
        //    .callback(|e: FocusEvent|Message::DisplayNationalityInfoText);
        let nationality_onchange_callback = self
            .component_link
            .callback(|e: ChangeData| Message::UpdateNationalityField(e));


        let previously_on_germany_onclick_callback = self
            .component_link
            .callback(|_| Message::UpdatePreviouslyOnGermany);

        let oninput_first_name = self
            .component_link
            .callback(|e: InputData| Message::UpdateFirstNameField(e.value));
        let oninput_last_name = self
            .component_link
            .callback(|e: InputData| Message::UpdateLastNameField(e.value));
        let oninput_email = self
            .component_link
            .callback(|e: InputData| Message::UpdateEmailField(e.value));
        let oninput_dob = self
            .component_link
            .callback(|e: InputData| Message::UpdateDoBField(e.value));

        html!{
            <div class="uk-grid-small">
                <fieldset class="uk-fieldset",>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"First Name"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                            id="form-stacked-text",
                            type="text",
                            value=&self.first_name_field,
                            oninput=oninput_first_name
                            placeholder={"First Name..."}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Last Name"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                             id="form-stacked-text",
                             type="text",
                             value=&self.last_name_field,
                             oninput=oninput_last_name,
                             placeholder={"Last Name..."}/>
                        </div>
                    </div>
                    <NationalityOptionsList onchange=nationality_onchange_callback />
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"E-mail"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                             id="form-stacked-text",
                             type="text",
                             value=&self.email_field,
                             oninput=oninput_email,
                             placeholder={"E-mail Address..."}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Year of Birth"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                            id="form-stacked-text",
                            type="text",
                            value=&self.dob_field,
                            oninput=oninput_dob,
                            placeholder={"YYYY"}/>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Has the candidate lived in Germany before?"}</label>
                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-radio",
                             type="radio",
                             checked=!self.on_germany_before_field,
                             onclick=previously_on_germany_onclick_callback.clone(), />{" Yes"}</label>
                            <label><input class="uk-radio",
                             type="radio",
                             checked=self.on_germany_before_field,
                             onclick=previously_on_germany_onclick_callback, />{" No"}</label>
                        </div>
                    </div>
                    <button class="uk-button uk-button-primary",
                        type="submit",
                        disabled=self.qualifications_button_disabled,
                        onclick=onclick>{"Qualifications"}</button>
                </fieldset>
            </div>
        }
    }

    fn qualifications(&self) -> Html{
        let onclick = self.component_link.callback(|ev:ClickEvent| Message::EmploymentDetailsRequest);
        let profession_onchange_callback = self
            .component_link
            .callback(|e: ChangeData| Message::UpdateProfessionField(e));

        let education_onclick_callback = self
            .component_link
            .callback(|_| Message::UpdateEducationField);

        let oninput_experience = self
            .component_link
            .callback(|e: InputData| Message::UpdateExperienceField(e.value));


        html!{
            <div class="uk-grid-small">
                <fieldset class="uk-fieldset",>
                    <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">{"Degree of Education"}</label>
                        <select class="uk-select",
                        onchange=education_onclick_callback>
                            <option>{"University Degree"}</option>
                            <option>{"No University Degree"}</option>
                        </select>
                    </div>
                    <ProfessionOptionsList onchange=profession_onchange_callback/>

                     <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Years of Experience"}</label>
                        <div class="uk-form-controls">
                            <input class="uk-input",
                            id="form-stacked-text",
                            type="text",
                            value=&self.experience_field,
                            oninput=oninput_experience,
                            placeholder={"##"}/>
                        </div>
                    </div>

                    <button class="uk-button uk-button-primary",
                    type="submit",
                    disabled=self.qualifications_button_disabled,
                    onclick=onclick>{"Employment Details"}</button>
                </fieldset>
            </div>

        }
    }

    fn employment_details(&self) -> Html{
        let onclick = self.component_link.callback(|ev:ClickEvent| Message::SummaryRequest);

        let visa_type_onchange_callback = self
            .component_link
            .callback(|e: ChangeData| Message::UpdateVisaTypeField(e));

        let work_location_onchange_callback = self
            .component_link
            .callback(|e: ChangeData| Message::UpdateWorkLocationField(e));

        let oninput_salary = self
            .component_link
            .callback(|e: InputData| Message::UpdateSalaryField(e.value));

        html!{
            <div class="uk-grid-small">
                <fieldset class="uk-fieldset",>
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">{"Yearly Salary"}</label>
                        <div class="uk-form-controls">
                            <div class="uk-inline uk-width-expand">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: ">{"€"}</span>
                                 <input class="uk-input",
                                id="form-stacked-text",
                                type="text",
                                value=&self.salary_field,
                                oninput=oninput_salary,
                                placeholder={"######.##"}/>
                            </div>
                        </div>
                    </div>

                    <VisaTypeOptionsList onchange=visa_type_onchange_callback/>
                    <WorkLocationOptionsList onchange=work_location_onchange_callback/>

                    <button class="uk-button uk-button-primary",
                    type="submit",
                    onclick=onclick>{"Confirm & Create Profile"}</button>
                </fieldset>
            </div>
        }
    }

    fn show_nationality_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Nationality"}</h4>
                <p>{"As some countries have bilateral agreements, you may qualify for a fast track process. Brazilian applicants are eligible. This saves you a lot of time!"}</p>
            </div>
        }
    }

    fn show_age_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Year of birth"}</h4>
                <p>{"This is needed for several application forms, also there are additional requirements for employees over 45. No problem in your case!"}</p>
            </div>
        }
    }

    fn show_previous_german_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Has the candidate lived in Germany before?"}</h4>
                <p>{"This determines the type of Visa you can apply for. We will preselect the Visa types your applicant qualifies for."}</p>
            </div>
        }
    }

    fn show_experience_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Years of Experience"}</h4>
                <p>{"Some Visa require a certain number of years of professional work experience. Your applicant has 5 years of experience and might qualify for a fast track visa."}</p>
            </div>
        }
    }

    fn show_profession_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Profession"}</h4>
                <p>{"You are in luck! The profession you selected is recognized in Germany! You do not need to worry about the qualification of your candidate."}</p>
            </div>
        }
    }

    fn show_education_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Degree of Education"}</h4>
                <p>{"Your candidate has lower salary requirements without a University degree."}</p>
            </div>
        }
    }

    fn show_salary_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Yearly Salary"}</h4>
                <p>{"The salary you typed is perfect to keep going in the process."}</p>
                <p>{"If your Employee is older than 45, the annual salary must be 45.540 Euro or higher. For the EU Blue Card minimum salary is 55.200 Euro."}</p>
            </div>
        }
    }

    fn show_visa_type_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Visa Type"}</h4>
                <p>{"Your candidate qualifies for a Visa for skilled workforce and the Expedited procedure for qualified professionals."}</p>
                <p>{"Congratulations, your employee is eligible for the fast track procedure."}</p>
            </div>
        }
    }

    fn show_work_location_info_text(&self) -> Html{
        html!{
            <div>
                <h4>{"Region of Employment"}</h4>
                <p>{"Nice! The Region you selected gives bonuses to employers hiring from abroad."}</p>
            </div>
        }
    }


    fn update_personal_info_button_state(&mut self) {
        self.qualifications_button_disabled = self.first_name_field.is_empty() ||
            self.last_name_field.is_empty() ||
            self.dob_field.is_empty() ||
            self.email_field.is_empty();
    }

    fn update_qualifications_button_state(&mut self) {
        self.qualifications_button_disabled = self.experience_field.is_empty();
    }

    fn update_employment_info_button_state(&mut self) {
        self.employment_details_button_disabled = self.salary_field.is_empty();
    }
}
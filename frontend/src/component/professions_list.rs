use crate::error::Error;
use crate::service::catalogs::{
    ProfessionsCatalogService
};
use yew::{Callback, Component, ComponentLink, ShouldRender, services::fetch::FetchTask, html, Html, Properties, ChangeData};
use webapp::protocol::response::ProfilesList;
use webapp::protocol::response::ProfessionsListResponse;
use crate::api::Response;


/// Data Model for the Professions dropdown
pub struct ProfessionOptionsList{
    professions_service: ProfessionsCatalogService,
    profession_service_response: Callback<Result<ProfessionsListResponse, Error>>,
    professions_list: Option<ProfessionsListResponse>,
    fetch_task: Option<FetchTask>,
    props: Props,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub onchange: Callback<ChangeData>,
}

/// Available message types to process
pub enum Message{
    LoadProfessionsList(Result<ProfessionsListResponse, Error>),
}

impl Component for ProfessionOptionsList{
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self{
        Self{
            professions_service: ProfessionsCatalogService::new(),
            profession_service_response: link.callback(Message::LoadProfessionsList),
            professions_list: None,
            fetch_task: None,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        self.fetch_task = Some(self.professions_service.get_all(self.profession_service_response.clone()));
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender{ false }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg {
            Message::LoadProfessionsList(Ok(professions_list)) => {
                self.professions_list = Some(professions_list);
            }
            Message::LoadProfessionsList(Err(_)) =>{
                self.professions_list = None;
            }
        }
        true
    }

    fn view(&self) -> Html{
        html! {
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">{"Profession"}</label>
                <select class="uk-select",
                onchange=&self.props.onchange>
                {
                    if let Some(professions_list) = &self.professions_list{
                        html!{
                            <>
                                {for professions_list.0.iter().map(|option|{
                                    html!{
                                    <>
                                        <option value=&option.id>{&option.description}</option>
                                    </>
                                    }
                                })}
                            </>
                        }
                    } else {
                        html! {
                            <>
                                <option value={-1}>{"No options here ..."}</option>
                            </>
                        }
                    }
                }
                </select>
            </div>
        }
    }
}
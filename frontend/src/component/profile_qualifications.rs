use yew::{Callback, Component, html, prelude::*, ComponentLink, Bridge, ShouldRender, Html, Bridged};
use yew_router::agent::RouteAgent;
use crate::service::uikit::UIkitService;
use webapp::protocol::request::CreateProfileRequest;
use crate::component::professions_list::ProfessionOptionsList;

pub struct ProfileQualificationsChildComponent{
    router_agent: Box<dyn Bridge<RouteAgent>>,
    uikit_service: UIkitService,
    profession_id: i32,
    degree_education: i32,
    inputs_disabled: bool,
    employment_button_disabled: bool,
    props: Props,
}

pub enum Message{
    Ignore,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub step: i32,
    pub profile_details: CreateProfileRequest,
}

impl Component for ProfileQualificationsChildComponent{
    type Message = Message;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self{
        let mut router_agent = RouteAgent::bridge(link.callback(|_| Message::Ignore));

        Self{
            router_agent,
            uikit_service: UIkitService::new(),
            profession_id: 0,
            degree_education: 0,
            inputs_disabled: false,
            employment_button_disabled: false,
            props,
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender{
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender{
        match msg{
            Message::Ignore => {}
        }
        true
    }

    fn view(&self) -> Html{
        html!{
            <form class="uk-grid-small">
                <fieldset class="uk-fieldset",>
                    <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">{"Degree of Education"}</label>
                        <select class="uk-select">
                            <option>{"No University Degree"}</option>
                            <option>{"Option 02"}</option>
                        </select>
                    </div>
                    <ProfessionOptionsList />

                    <button class="uk-button uk-button-primary",
                    type="submit",
                    >{"Employment Details"}</button>
                </fieldset>
            </form>

        }
    }
}

impl ProfileQualificationsChildComponent {
    fn update_button_state(&mut self) {
        //self.login_button_disabled = self.username.is_empty() || self.password.is_empty();
    }
}
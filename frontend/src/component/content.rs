//! The Main Content component

use crate::{
    component::applicant_card::ApplicantCardComponent,
    api::Response,
    route::RouterTarget,
    service::{
        cookie::CookieService,
        session_timer::{self, SessionTimerAgent},
        uikit::{NotificationStatus, UIkitService},
    },
    string::{REQUEST_ERROR, RESPONSE_ERROR, TEXT_ADD_NEW_APPLICANT},
    SESSION_COOKIE,
};
use log::{error, info, warn};
use webapp::{
    protocol::{model::UserSession, request, response},
    API_URL_LOGOUT,
    API_URL_PROFILES
};
use yew::{agent::Bridged, format::Json, html, prelude::*, services::fetch::FetchTask};
use yew_router::agent::{RouteAgent, RouteRequest::ChangeRoute};
use webapp::protocol::request::FindProfilesForHR;
use webapp::protocol::response::{ProfilesList, User};
use crate::service::profile::ProfileService;
use uuid::Uuid;
use crate::error::Error;
use crate::types::UserInfo;
use crate::service::user::UserService;

/// Data Model for the Content component
pub struct ContentComponent {
    user_service: UserService,
    user_data: Option<response::User>,
    user_response: Callback<Result<User, Error>>,
    profiles: ProfileService,
    profiles_list: Option<response::ProfilesList>,
    component_link: ComponentLink<ContentComponent>,
    response: Callback<Result<ProfilesList, Error>>,
    cookie_service: CookieService,
    fetch_task: Option<FetchTask>,
    logout_button_disabled: bool,
    router_agent: Box<dyn Bridge<RouteAgent<()>>>,
    session_timer_agent: Box<dyn Bridge<SessionTimerAgent>>,
    uikit_service: UIkitService,
}

/// Available message types to process
pub enum Message {
    Fetch(Response<response::Logout>),
    LoadProfiles(Result<ProfilesList, Error>),
    Ignore,
    LogoutRequest,
    LoadProfilesRequest,
    LoadUserInfo(Result<User, Error>),
    LoadCreateProfileComponent
}

impl Component for ContentComponent {
    type Message = Message;
    type Properties = ();

    /// Initialization routine
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        // Guard the authentication
        let mut router_agent = RouteAgent::bridge(link.callback(|_| Message::Ignore));
        let cookie_service = CookieService::new();
        let mut session_timer_agent = SessionTimerAgent::bridge(link.callback(|_| Message::Ignore));
        if cookie_service.get(SESSION_COOKIE).is_err() {
            info!("No session token found, routing back to login");
            router_agent.send(ChangeRoute(RouterTarget::Login.into()));
        } else {
            // Start the timer to keep the session active
            session_timer_agent.send(session_timer::Request::Start);
        }

        // Return the component
        Self {
            user_service: UserService::new(),
            user_data: None,
            user_response: link.callback(Message::LoadUserInfo),
            profiles: ProfileService::new(),
            profiles_list: None,
            component_link: link.clone(),
            response: link.callback(Message::LoadProfiles),
            cookie_service,
            fetch_task: None,
            logout_button_disabled: false,
            router_agent,
            session_timer_agent,
            uikit_service: UIkitService::new(),
        }
    }

    fn mounted(&mut self) -> ShouldRender{
        if let Ok(token) = self.cookie_service.get(SESSION_COOKIE) {
                self.fetch_task = Some(self.user_service.who_am_i(token, self.user_response.clone()));
        }else {
            // It should not happen but in case there is no session cookie on logout, route
            // back to login
            error!("No session cookie found");
            self.router_agent
                .send(ChangeRoute(RouterTarget::Login.into()));
        }
        false
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    /// Called everytime when messages are received
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Message::LogoutRequest => {
                if let Ok(token) = self.cookie_service.get(SESSION_COOKIE) {
                    self.fetch_task = fetch! {
                        request::LogoutSession(token) => API_URL_LOGOUT,
                        self.component_link, Message::Fetch,
                        || {
                            // Disable user interaction
                            self.logout_button_disabled = true;
                        },
                        || {
                            error!("Unable to create logout request");
                            self.uikit_service
                                .notify(REQUEST_ERROR, &NotificationStatus::Danger);
                        }
                    };
                } else {
                    // It should not happen but in case there is no session cookie on logout, route
                    // back to login
                    error!("No session cookie found");
                    self.router_agent
                        .send(ChangeRoute(RouterTarget::Login.into()));
                }
            }

            Message::LoadUserInfo(Ok(user_info)) =>{
                self.fetch_task = Some(self.profiles.by_recruiter_id(user_info.id, self.response.clone()));
                self.user_data = Some(user_info);
            }
            Message::LoadUserInfo(Err(_)) =>{
                self.fetch_task = None;
            }

            Message::LoadProfiles(Ok(profiles_list)) => {
                self.profiles_list = Some(profiles_list);
                self.fetch_task = None;
            }
            Message::LoadProfiles(Err(_)) => {
                self.fetch_task = None;
            }

            //The message to load the profiles
            Message::LoadProfilesRequest => {
                //self.fetch_task = Some(self.profiles.by_recruiter_id(Uuid::parse_str("eff81d9e-7118-48cf-a72b-7e479b4840e6").unwrap(), self.response.clone()));;
            }

            // The message for all fetch responses
            Message::Fetch(response) => {
                let (meta, Json(body)) = response.into_parts();

                // Check the response type
                if meta.status.is_success() {
                    match body {
                        Ok(response::Logout) => info!("Got valid logout response"),
                        _ => {
                            warn!("Got wrong logout response");
                            self.uikit_service
                                .notify(RESPONSE_ERROR, &NotificationStatus::Danger);
                        }
                    }
                } else {
                    warn!("Logout failed with status: {}", meta.status);
                }

                // Remove the existing cookie
                self.cookie_service.remove(SESSION_COOKIE);
                self.session_timer_agent.send(session_timer::Request::Stop);
                self.router_agent
                    .send(ChangeRoute(RouterTarget::Login.into()));
                self.logout_button_disabled = true;

                // Remove the ongoing task
                self.fetch_task = None;
            }
            Message::LoadCreateProfileComponent => {
                self.router_agent.send(ChangeRoute(RouterTarget::ProfileCreation.into()));
            }
            Message::Ignore => {}
        }
        true
    }

    fn view(&self) -> Html {
        let onclick = self.component_link.callback(|_| Message::LogoutRequest);
        let create_new_profile = self.component_link.callback((|_| Message::LoadCreateProfileComponent));

        html! {
            <div class="uk-section uk">
                <div class="uk-card-header">
                    <div class="uk-grid uk-flex-left">
                        <img src="images/logo_skillerator.svg" alt="" width="173" height="77"/>
                    </div>
                </div>
                <div class="uk-card-body">
                    <div class="uk-grid uk-flex-left">
                        <div class="uk-width-auto">
                            <ul class="uk-iconnav uk-iconnav-vertical uk-list-large">
                                <li><a href="#" uk-icon="icon: home; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: cog; ratio: 2"></a></li>
                                <li><a href="#" uk-icon="icon: bell; ratio: 2"></a></li>
                                <li><a onclick=onclick uk-icon="icon: sign-out; ratio: 2",
                                ></a></li>
                            </ul>
                        </div>
                        <div class="uk-width-expand">
                            <button onclick=create_new_profile
                            class="uk-button uk-button-primary">
                                        {TEXT_ADD_NEW_APPLICANT}
                            </button>
                            <hr/>
                            <div class="uk-grid-match uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-grid">
                                {
                                    if let Some(profiles_list) = &self.profiles_list{
                                        html!{
                                            <>
                                                {for profiles_list.0.iter().map(|profile| {
                                                        html! { <ApplicantCardComponent profile=profile /> }
                                                })}
                                            </>
                                        }
                                    } else {
                                        html! {
                                            <div class="article-preview">{ "No profiles are here... yet." }</div>
                                        }
                                    }
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-card-footer">

                </div>

            </div>
        }
    }
}

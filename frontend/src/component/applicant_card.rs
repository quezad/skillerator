//! The Applicant card component

use crate::{
    api::Response,
    route::RouterTarget,
    service::{
        cookie::CookieService,
        uikit::{NotificationStatus, UIkitService},
    },
    string::{PLACEHOLDER, AUTHENTICATION_ERROR, RESPONSE_ERROR, REQUEST_ERROR},
    SESSION_COOKIE,
};
use log::{error, info, warn};
use webapp::{
    protocol::{model::UserSession, request::LoginCredentials, response::Login},
    API_URL_LOGIN_CREDENTIALS,
};
use yew::{format::Json, html, prelude::*, services::fetch::FetchTask};
use yew_router::agent::{RouteAgent, RouteRequest::ChangeRoute};
use yew_router::prelude::*;
use webapp::protocol::model::Profile;

/// Data Model for the Applicant Card component
pub struct ApplicantCardComponent {
    component_link: ComponentLink<ApplicantCardComponent>,
    cookie_service: CookieService,
    fetch_task: Option<FetchTask>,
    inputs_disabled: bool,
    login_button_disabled: bool,
    password: String,
    router_agent: Box<dyn Bridge<RouteAgent<()>>>,
    uikit_service: UIkitService,
    username: String,
    props: Props,
}

/// Available message types to process
pub enum Message {
    Fetch(Response<Login>),
    Ignore,
    LoginRequest,
    UpdatePassword(String),
    UpdateUsername(String),
}

#[derive(Properties, Clone)]
pub struct Props {
    pub profile: Profile,
}

impl Component for ApplicantCardComponent {
    type Message = Message;
    type Properties = Props;

    /// Initialization routine
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        // Return the component
        Self {
            cookie_service: CookieService::new(),
            fetch_task: None,
            inputs_disabled: false,
            login_button_disabled: true,
            password: String::new(),
            router_agent: RouteAgent::bridge(link.callback(|_| Message::Ignore)),
            component_link: link,
            uikit_service: UIkitService::new(),
            username: String::new(),
            props
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    /// Called everytime when messages are received
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let nationality_class = format!("flag-icon {}", &self.props.profile.detail_country_icon);

        html! {
            <div>
                <div class="uk-card uk-card-default uk-card-body uk-height-match">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-grid-match uk-grid">
                            <div class="uk-width-1-3">
                                {
                                    if &self.props.profile.detail_first_name == "Antônia"{
                                        html!{<img style="object-fit: contain" class="uk-border-circle" src="images/antonia.svg"/>}
                                    } else if &self.props.profile.detail_first_name == "Marcos"{
                                        html!{<img style="object-fit: contain" class="uk-border-circle" src="images/marcos.svg"/>}
                                    } else{
                                        html!{<img style="object-fit: contain" class="uk-border-circle" src="images/ekaterina.svg"/>}
                                    }
                                }

                            </div>
                            <div class="uk-width-2-3">
                                <h3 class="uk-card-title uk-margin-remove-bottom">
                                    {format!("{} {}", &self.props.profile.detail_first_name, &self.props.profile.detail_last_name)}
                                </h3>
                            </div>
                        </div>
                        <div class="uk-width-expand">
                            <p class="uk-text-meta uk-margin-remove-top"><span style="margin-right:0.5em" class=nationality_class></span>{&self.props.profile.detail_country_name}</p>
                        </div>
                    </div>
                    <div class="uk-card-body">
                        <p>
                            {if self.props.profile.detail_for_applicant{
                                {html!{
                                    <>
                                        <span class="uk-icon-button" style="margin-right:0.5em" uk-icon="icon: user;"></span>
                                    </>
                                }}
                            }else{
                                {html!{
                                    <>
                                        <span class="uk-icon-button" style="margin-right:0.5em" uk-icon="icon: bolt;"></span>
                                    </>
                                }}
                            }}
                            {&self.props.profile.detail_current_step}
                        </p>
                    </div>
                    <div class="uk-card-footer">
                        <a href="#" class="uk-button uk-button-text">{" See More..."}</a>
                    </div>
                </div>
            </div>
        }
    }
}
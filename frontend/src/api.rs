//! Api related helpers and utilities

use anyhow::Result;
use yew::{format::Json, services::fetch::Response as FetchResponse, Callback};
use yew::services::fetch::{FetchService, FetchTask, Request};
use crate::error::Error;
use serde::{Deserialize, Serialize};
use yew::format::{Text, Nothing};
use serde_json;
use crate::types::ErrorInfo;
use log::debug;

const API_ROOT: &str = env!("API_URL");

/// A generic response type of the API
pub type Response<T> = FetchResponse<Json<Result<T>>>;

#[macro_export]
/// Generic API fetch macro
macro_rules! fetch {
    ($request:expr => $api:expr, $link:expr, $msg:expr, $succ:expr, $err:expr) => {
        match ::yew::services::fetch::Request::post(env!("API_URL").to_owned() + $api)
            .header("Content-Type", "application/json")
            .body(Json(&$request))
        {
            Ok(body) => {
                $succ();
                ::yew::services::fetch::FetchService::new()
                    .fetch_binary(body, $link.callback($msg))
                    .ok()
            }
            Err(_) => {
                $err();
                None
            }
        };
    };
}

/// Http request
#[derive(Default, Debug)]
pub struct Requests {
    fetch: FetchService,
}

impl Requests {
    pub fn new() -> Self {
        Self {
            fetch: FetchService::new(),
        }
    }

    /// build all kinds of http request: post/get/delete etc.
    pub fn builder<B, T>(
        &mut self,
        method: &str,
        url: String,
        body: B,
        callback: Callback<Result<T, Error>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Into<Text> + std::fmt::Debug,
    {
        let handler = move |response: FetchResponse<Text>| {
            if let (meta, Ok(data)) = response.into_parts() {
                debug!("Response: {:?}", data);
                if meta.status.is_success() {
                    let data: Result<T, _> = serde_json::from_str(&data);
                    if let Ok(data) = data {
                        callback.emit(Ok(data))
                    } else {
                        callback.emit(Err(Error::DeserializeError))
                    }
                } else {
                    match meta.status.as_u16() {
                        401 => callback.emit(Err(Error::Unauthorized)),
                        403 => callback.emit(Err(Error::Forbidden)),
                        404 => callback.emit(Err(Error::NotFound)),
                        500 => callback.emit(Err(Error::InternalServerError)),
                        422 => {
                            let data: Result<ErrorInfo, _> = serde_json::from_str(&data);
                            if let Ok(data) = data {
                                callback.emit(Err(Error::UnprocessableEntity(data)))
                            } else {
                                callback.emit(Err(Error::DeserializeError))
                            }
                        }
                        _ => callback.emit(Err(Error::RequestError)),
                    }
                }
            } else {
                callback.emit(Err(Error::RequestError))
            }
        };

        let url = format!("{}{}", API_ROOT, url);
        let mut builder = Request::builder()
            .method(method)
            .uri(url.as_str())
            .header("Content-Type", "application/json");
        /*if let Some(token) = get_token() {
            builder = builder.header("Authorization", format!("Token {}", token));
        }*/
        let request = builder.body(body).unwrap();
        debug!("Request: {:?}", request);

        self.fetch.fetch(request, handler.into()).unwrap()
    }

    /// Delete request
    pub fn delete<T>(&mut self, url: String, callback: Callback<Result<T, Error>>) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
    {
        self.builder("DELETE", url, Nothing, callback)
    }

    /// Get request
    pub fn get<T>(&mut self, url: String, callback: Callback<Result<T, Error>>) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
    {
        self.builder("GET", url, Nothing, callback)
    }

    /// Post request with a body
    pub fn post<B, T>(
        &mut self,
        url: String,
        body: B,
        callback: Callback<Result<T, Error>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Serialize,
    {
        let body: Text = Json(&body).into();
        self.builder("POST", url, body, callback)
    }

    /// Put request with a body
    pub fn put<B, T>(
        &mut self,
        url: String,
        body: B,
        callback: Callback<Result<T, Error>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Serialize,
    {
        let body: Text = Json(&body).into();
        self.builder("PUT", url, body, callback)
    }
}

/// Set limit for pagination
pub fn limit(count: u32, p: u32) -> String {
    let offset = if p > 0 { p * count } else { 0 };
    format!("limit={}&offset={}", count, offset)
}
table! {
    /// Representation of the `detailed_profiles` table.
    detailed_profiles (detail_recruiter_id, detail_id, detail_applicant_id) {
        detail_id -> Uuid,
        detail_applicant_id -> Uuid,
        detail_recruiter_id -> Uuid,
        detail_first_name -> Varchar,
        detail_last_name -> Varchar,
        detail_date_of_birth -> Date,
        detail_years_experience -> Numeric,
        detail_work_field -> Varchar,
        detail_salary -> Numeric,
        detail_visa_type -> Varchar,
        detail_work_location -> Varchar,
        detail_country_icon -> Varchar,
        detail_country_name -> Varchar,
        detail_current_step -> Varchar,
        detail_for_applicant -> Bool,
    }
}
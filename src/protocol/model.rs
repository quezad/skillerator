//! Basic models
#[cfg(feature = "backend")]
use crate::schema::{
    user,
    work_locations,
    work_fields,
    visa_types,
    countries,
    profiles
};

#[cfg(feature = "backend")]
use crate::views::detailed_profiles;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDate, NaiveDateTime};
use bigdecimal::BigDecimal;
use uuid::Uuid;

#[cfg_attr(feature = "backend", derive(Queryable, Identifiable, Associations, Insertable)) ]
#[cfg_attr(feature = "backend", table_name = "user") ]
#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// A session representation
pub struct User {
    /// The user id
    pub id: Uuid,
    /// The username to search for
    pub email: String,
    /// The password to search for
    #[serde(skip_serializing)]
    pub password: String,
    /// The role definition
    pub is_applicant: bool,
    /// The generated session token
    pub session_token: Option<String>,
    /// The creation time
    pub created_at: NaiveDateTime,
    ///The update time
    pub updated_at: Option<NaiveDateTime>,
}

#[cfg_attr(feature = "backend", derive(Queryable, Identifiable, Associations, AsChangeset)) ]
#[cfg_attr(feature = "backend", table_name = "user") ]
#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// A session representation
pub struct UserSession {
    /// The user id
    pub id: Uuid,
    /// The username to search for
    pub email: String,
    /// The password to search for
    pub password: String,
    /// The generated session token
    pub session_token: Option<String>,
    /// The role definition
    pub is_applicant: bool,
}

#[cfg_attr(feature = "backend", derive(Identifiable, Insertable, Queryable))]
#[cfg_attr(feature = "backend", table_name = "visa_types")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
/// A visa type catalog
pub struct VisaType{
    /// The visa type id
    pub id: i32,
    /// The visa type description
    pub description: String
}

#[cfg_attr(feature = "backend", derive(Identifiable, Insertable, Queryable))]
#[cfg_attr(feature = "backend", table_name = "countries")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
/// A countries catalog
pub struct Country{
    /// The country id
    pub id: String,
    /// The country flag icon
    pub icon: String,
    /// The country description
    pub description: String,
}

#[cfg_attr(feature = "backend", derive(Identifiable, Insertable, Queryable))]
#[cfg_attr(feature = "backend", table_name = "work_fields")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
/// A work field catalog
pub struct WorkField{
    /// The work field id
    pub id: i32,
    /// The work field description
    pub description: String
}

#[cfg_attr(feature = "backend", derive(Identifiable, Insertable, Queryable))]
#[cfg_attr(feature = "backend", table_name = "work_locations")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
/// A work location catalog
pub struct WorkLocation{
    /// The work location id
    pub id: i32,
    /// The work location description
    pub description: String
}

#[cfg_attr(feature = "backend", derive(Identifiable, Queryable, Associations))]
#[cfg_attr(feature = "backend", table_name = "detailed_profiles")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
#[cfg_attr(feature = "backend", primary_key(detail_id, detail_applicant_id, detail_recruiter_id))]
#[cfg_attr(feature = "backend", belongs_to(User, foreign_key = "detail_recruiter_id"))]
/// A profile representation
pub struct Profile{
    /// The actual profile id
    pub detail_id: Uuid,
    /// The profile applicant associated id
    pub detail_applicant_id: Uuid,
    /// The profile hr associated id
    pub detail_recruiter_id: Uuid,
    /// The applicant first name
    pub detail_first_name: String,
    /// The applicant last name
    pub detail_last_name: String,
    /// The applicant date of birth
    pub detail_date_of_birth: NaiveDate,
    /// The applicant's years of experience
    pub detail_years_experience: BigDecimal,
    /// The applicant's field of work
    pub detail_work_field: String,
    /// The applicant's salary expectations
    pub detail_salary: BigDecimal,
    /// The applicant's visa type
    pub detail_visa_type: String,
    /// The applicant's work location
    pub detail_work_location: String,
    /// The applicant's nationality icon
    pub detail_country_icon: String,
    /// The applicant's nationality
    pub detail_country_name: String,
    /// The process latest step
    pub detail_current_step: String,
    /// Is an applicant or recruiter task?
    pub detail_for_applicant: bool,
}


#[cfg_attr(feature = "backend", derive(Identifiable, Queryable, Associations))]
#[cfg_attr(feature = "backend", table_name = "profiles")]
#[derive( Serialize, Deserialize, Debug, Clone, PartialEq)]
#[cfg_attr(feature = "backend", primary_key(id, applicant_id, recruiter_id))]
/// A profile representation
pub struct ProfileObj {
    /// The applicant id
    pub applicant_id: Uuid,
    /// The recruiter id
    recruiter_id: Uuid,
    /// The profile id
    id: Uuid,
    /// The applicants first name
    first_name: Option<String>,
    /// The applicant's last name
    last_name: Option<String>,
    /// The applicant's date of birth
    date_of_birth: Option<NaiveDate>,
    /// The applicant's experience
    years_experience: Option<BigDecimal>,
    /// The profession id
    profession_id: Option<i32>,
    /// The salary expectations
    salary: Option<BigDecimal>,
    /// The visa type id
    visa_type_id: Option<i32>,
    /// The work location id
    work_location_id: Option<i32>,
    /// The country code
    country: Option<String>,
    /// The current process step
    process_step_id: Option<i32>,
}
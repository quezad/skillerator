//! Response specific implementations
use serde::{Deserialize, Serialize};
use chrono::NaiveDateTime;
use crate::protocol::model::Profile;
use uuid::Uuid;
use std::fmt;

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The login response
pub struct Login{
    /// The temporary session token
    pub session_token: String,
    /// The user role
    pub is_applicant: bool,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The logout response
pub struct Logout;

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The profile entry
pub struct ProfileItem {
    /// The profile id
    pub id: i32,
    /// The applicant first name
    pub first_name: Option<String>,
    /// The applicant last name
    pub last_name: Option<String>,
    /// The applicant date of birth
    pub date_of_birth: Option<NaiveDateTime>,
    /// The applicant's years of experience
    pub exp_years: Option<i32>,
    /// The applicant's field of work
    pub field_of_work: Option<String>,
    /// The applicant's salary expectations
    pub salary: Option<f64>,
    /// The applicant's visa type
    pub visa_type: Option<String>,
    /// The applicant's work location
    pub work_location: Option<String>,
    /// The applicant's country icon
    pub country_icon: Option<String>,
    /// The applicant's country
    pub country_name: Option<String>,
    /// The process step
    pub current_step: Option<String>,
    /// Is and applicant or recruiter task?
    pub for_applicant: Option<bool>
}

#[derive(Serialize, Deserialize, Debug)]
/// The profiles list
pub struct ProfilesList(pub Vec<Profile>);

#[derive(Serialize, Deserialize, Debug)]
/// The user object
pub struct User {
    /// The user id
    pub id: Uuid,
    /// The user email
    pub email: String,
    /// The user role
    pub is_applicant: bool,
}

#[derive(Serialize, Deserialize, Debug)]
/// The professions list response
pub struct ProfessionsListResponse(pub Vec<ProfessionOption>);

#[derive(Serialize, Deserialize, Debug)]
/// The professions list response
pub struct NationalitiesListResponse(pub Vec<NationalityOption>);

#[derive(Serialize, Deserialize, Debug)]
/// The visa types list response
pub struct VisaTypesListResponse(pub Vec<VisaTypeOption>);

#[derive(Serialize, Deserialize, Debug)]
/// The work locations list response
pub struct WorkLocationsListResponse(pub Vec<WorkLocationOption>);

#[derive(Serialize, Deserialize, Debug)]
/// The profession object
pub struct ProfessionOption{
    /// The profession id
    pub id: i32,
    /// The profession description
    pub description: String,
}

#[derive(Serialize, Deserialize, Debug)]
/// The visa type object
pub struct VisaTypeOption{
    /// The profession id
    pub id: i32,
    /// The profession description
    pub description: String,
}

#[derive(Serialize, Deserialize, Debug)]
/// The work locations object
pub struct WorkLocationOption{
    /// The profession id
    pub id: i32,
    /// The profession description
    pub description: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
/// The profession object
pub struct NationalityOption{
    /// The profession id
    pub id: String,
    /// The flag icon
    pub icon: String,
    /// The profession description
    pub description: String,
}

// Similarly, implement `Display` for `Point2D`
impl fmt::Display for NationalityOption {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Customize so only `x` and `y` are denoted.
        write!(f, "id: {}, icon: {}, description: {}", self.id, self.icon, self.description)
    }
}
//! Request messages
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use bigdecimal::BigDecimal;
use chrono::{NaiveDate, Utc};

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The credentials based login request
pub struct LoginCredentials {
    /// The username to login
    pub username: String,

    /// The password to login
    pub password: String,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The create applicant request
pub struct CreateApplicantRequest(pub String);

#[derive(Debug, PartialEq, Deserialize, Serialize, Clone)]
/// The create profile request
pub struct CreateProfileRequest{
    /// The applicant uuid
    pub applicant_id: Uuid,
    /// The recruiter uuid
    pub recruiter_id: Uuid,
    /// The applicant's first name
    pub first_name: String,
    /// The applicant's last name
    pub last_name: String,
    /// The applicant's date of birth
    pub date_of_birth: NaiveDate,
    /// The applicant's years of experience
    pub years_experience: BigDecimal,
    /// The applicant's profession id
    pub profession_id: i32,
    /// The applicant's salary
    pub salary: BigDecimal,
    /// The applicant's visa type id
    pub visa_type_id: i32,
    /// The applicant's work location id
    pub work_location_id: i32,
    /// The applicant's country iso
    pub country: String,
    /// The applicant's process_step_id
    pub process_step_id: i32,
}

impl Default for CreateProfileRequest{
    fn default() -> CreateProfileRequest{
        CreateProfileRequest{
            applicant_id: Default::default(),
            recruiter_id: Default::default(),
            first_name: "".to_string(),
            last_name: "".to_string(),
            date_of_birth: NaiveDate::from_ymd(1970, 1,1),
            years_experience: Default::default(),
            profession_id: 0,
            salary: Default::default(),
            visa_type_id: 0,
            work_location_id: 0,
            country: "".to_string(),
            process_step_id: 0
        }
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The session based login request
pub struct LoginSession(pub String);

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The logout request
pub struct LogoutSession(pub String);

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The profiles request
pub struct FindProfilesForHR(pub i32);

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// Utility who aim I method
pub struct FindUserBySessionTokenRequest(pub String);

#[derive(Debug, PartialEq, Deserialize, Serialize)]
/// The professions list request
pub struct LoadProfessionsListRequest();

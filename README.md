# WebApp.rs

[![Docs release](https://docs.rs/webapp/badge.svg)](https://docs.rs/webapp)
[![Docs release backend](https://docs.rs/webapp-backend/badge.svg)](https://docs.rs/webapp-backend)
[![Docs release frontend](https://docs.rs/webapp-frontend/badge.svg)](https://docs.rs/webapp-frontend)
[![Crates.io](https://img.shields.io/crates/v/webapp.svg)](https://crates.io/crates/webapp)
[![Crates.io](https://img.shields.io/crates/v/webapp-backend.svg)](https://crates.io/crates/webapp-backend)
[![Crates.io](https://img.shields.io/crates/v/webapp-frontend.svg)](https://crates.io/crates/webapp-frontend)

## Skillerator app completely written in Rust

The prototype can be accessed through the following link:
http://64.227.50.108:9080

# Considerations
- The process shown on the video is the only full implemented right now, so it's recommended to stick to that path for now.
- Some links are not implemented yet.
- In order to start the applicant registration process, the login is: 'paul@asap.de' with password: '123'.
- Whenever a new applicant is registered, a new account is created with the supplied email, and a default password '123'.
- If a applicant account is used to login into the system, it would redirect to an empty page, as this is still a work in progress.

The following build dependencies needs to be fulfilled to support the full
feature set of this application:

- [cargo-web](https://github.com/koute/cargo-web)
- [diesel_cli](https://github.com/diesel-rs/diesel)
- [postgresql (libpg)](https://www.postgresql.org/)
- A container runtime, like [podman](https://podman.io)

The app consist of a frontend and a backend. For getting started with hacking,
the backend can be tested via `make run-backend`, whereas the frontend can be
tested with `make run-frontend`. You can adapt the application configuration
within `Config.toml` if needed.

This installs build requirements, rust and cargo-web, on Ubuntu or Debian.
``` console
wget https://sh.rustup.rs -O rustup-init
sudo sh rustup-init -y
sudo apt-get install -y pkg-config libssl-dev
sudo cargo install cargo-web
```
This builds the project.
``` console
cd skillerator
make all
```
## Run

The image for the latest build of the project can be downloaded from:
`https://hub.docker.com/repository/docker/mquezada/skillerator_rust`

`make deploy` uses podman to start a PostgreSQL container and the Rust backend container.
If you wish to use docker instead of podman, set `CONTAINER_RUNTIME=podman` in the top of `Makefile`.
Edit `Config.toml` if needed to set the backend url and PostgreSQL credentials:
``` console
[server]
url = "http://127.0.0.1:30080"
...
[postgres]
host = "127.0.0.1"
username = "username"
password = ""
database = "database"
```
Ensure the runtime dependencies are installed, and the start the two containers.
``` console
sudo apt install -y postgresql-client
cargo install diesel_cli --no-default-features --features "postgres"
sudo make deploy
```
The application should now be accessible at
[`http://127.0.0.1:30080`](http://127.0.0.1:30080).
During development, you can start the containers separately, using 
`make run-app` to start only the rust backend container, and `run-postgres` to start only the PostgreSQL container.

If both the backend and frontend are running, you can visit the web application
at [`http://127.0.0.1:30080`](http://127.0.0.1:30080). After the successful
loading of the application you should see an authentication screen like this:


